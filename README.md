# Wings Project

# Project AtavismXI

### License Notice

**Original contributions to the Atavism project (i.e. not originating from the Darkstar project or Project Topaz) are governed by the GNU AGPLv3 (rather than GPL), which requires modified versions to be shared back, unless marked as dual-licensed by their respective authors. See the [LICENSE](LICENSE) file for more details.**

\*\*The new login server (Copyright (c) 2021 Wings Project) is not based on Topaz and is entirely under AGPLv3.

## Welcome to Project Atavism

Project Atavism is a server emulator for FFXI, aiming to reproduce the Chains of Promathia game experience as it was in February 2006.

### Website

https://www.atavismxi.com/

### Forums
https://www.wingsxi.com/forum/

### Discord

https://discord.gg/D8urYjmhDA

### Server

play.atavismxi.com

### Bug Reports

Bugs and issues pertaining to this code base should go to this project's bug tracker, which you can find here: https://gitlab.com/Grahf0085/atavism/-/issues

Before reporting issues please make sure that:

1. Your local tree is fully updated and that all server processes have been rebuilt form the latest code.
2. The database is in full sync with the latest source tree.
3. You are using the latest Atavism installer available in from our website.

Bug reports should include steps to reproduce the bug.

We do take issues detailing unimplemented features _if_ the feature is _retail behavior, era accurate_, and the issue report adequetly covers everything about that feature which is missing.

All of this helps speeding up the bugfix process!

### Pull Requests

**By submitting a Pull Request to Project Atavism, you agree to our [Limited Contributor License Agreement](CONTRIBUTOR_AGREEMENT.md)**

Commits should contain a descriptive name for what you are modifying

Check the contributing guide for the project style guide: [Contributing](CONTRIBUTING.md)

You must _test your code_ before committing changes/submitting a pull request. You break it you fix it!

Submit early and often! Try not to touch the PR too much after its submission (unless we request changes).

Remember to check back for any feedback, and drop a comment once requested changes have been made (if there are any).
