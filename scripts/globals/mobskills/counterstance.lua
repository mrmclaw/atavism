---------------------------------------------
-- Ability: Counterstance
-- Increases chance to counter but lowers defense.
-- Recast Time: 5:00
-- Duration: 5:00
---------------------------------------------
require("scripts/globals/monstertpmoves")
require("scripts/globals/settings")
require("scripts/globals/status")
---------------------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
   local typeEffect = tpz.effect.COUNTERSTANCE
    skill:setMsg(MobBuffMove(mob, typeEffect, 45, 0, 300))
    return typeEffect
end
