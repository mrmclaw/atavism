-----------------------------------
-- Ability: Light Shot
-- Dispel 1 effect from target
-- Apply Bio or Blind
-----------------------------------
require("scripts/globals/ability")
require("scripts/globals/magic")
require("scripts/globals/status")
require("scripts/globals/monstertpmoves")
-----------------------------------

function onMobSkillCheck(target, mob, skill)
    return 0
end

function onMobWeaponSkill(target, mob, skill)
	local effects = {}
    local dia = target:getStatusEffect(tpz.effect.DIA)
    if dia ~= nil then
        table.insert(effects, dia)
    end
    local threnody = target:getStatusEffect(tpz.effect.THRENODY)
    if threnody ~= nil and threnody:getSubPower() == tpz.mod.DARKRES then
        table.insert(effects, threnody)
    end

    if #effects > 0 then
        local effect = effects[math.random(#effects)]
        local effectDuration = effect:getDuration()
        local startTime = effect:getStartTime()
        local tick = effect:getTick()
        local power = effect:getPower()
        local subpower = effect:getSubPower()
        local tier = effect:getTier()
        local effectId = effect:getType()
        local subId = effect:getSubType()
        power = power * 1.5
        subpower = subpower * 1.5
        target:delStatusEffectSilent(effectId)
        target:addStatusEffect(effectId, power, tick, effectDuration, subId, subpower, tier)
        local newEffect = target:getStatusEffect(effectId)
        newEffect:setStartTime(startTime)
    end
	
	if target:getStatusEffect(tpz.effect.SLEEP_I) ~= nil then
		skill:setMsg(tpz.msg.basic.SKILL_NO_EFFECT)
	else 
		skill:setMsg(tpz.msg.basic.SKILL_ENFEEB_IS)
		target:addStatusEffect(tpz.effect.SLEEP_I, 1, 0, (math.random(6,27)))
	end

     return tpz.effect.SLEEP_I
end
