-----------------------------------------
-- Spell: Dia II
-- Lowers an enemy's defense and gradually deals light elemental damage.
-----------------------------------------
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/utils")
require("scripts/globals/msg")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    local basedmg = caster:getSkillLevel(tpz.skill.ENFEEBLING_MAGIC) / 4
    local params = {}
    params.dmg = basedmg
    params.multiplier = 3
    params.skillType = tpz.skill.ENFEEBLING_MAGIC
    params.attribute = tpz.mod.MND
    params.hasMultipleTargetReduction = false
    params.diff = caster:getStat(tpz.mod.MND) - target:getStat(tpz.mod.MND)
    params.bonus = 1.0

    -- Calculate raw damage
    local dmg = calculateMagicDamage(caster, target, spell, params)
    -- Softcaps at 8, should always do at least 1
    dmg = utils.clamp(dmg, 1, 8)
    -- Get resist multiplier (1x if no resist)
    local resist = applyResistance(caster, target, spell, params)
    -- Get the resisted damage
    dmg = dmg * resist
    -- Add on bonuses (staff/day/weather/jas/mab/etc all go in this function)
    dmg = addBonuses(caster, spell, target, dmg)
    -- Add in target adjustment
    dmg = adjustForTarget(target, dmg, spell:getElement())
    -- Add in final adjustments including the actual damage dealt
    local final = finalMagicAdjustments(caster, target, spell, dmg)

    -- Calculate duration and bonus
    local duration = calculateDuration(120, spell:getSkillType(), spell:getSpellGroup(), caster, target)
    local dotBonus = caster:getMod(tpz.mod.DIA_DOT) -- Dia Wand

    spell:setMsg(tpz.msg.basic.MAGIC_DMG) -- hit for initial damage

    local bio = target:getStatusEffect(tpz.effect.BIO)

    if  bio == nil then -- if no bio, add dia dot
        target:addStatusEffect(tpz.effect.DIA, 2 + dotBonus, 3, duration, 0, 15, 2)
    elseif
        bio:getSubPower() == 10 or
        (BIO_OVERWRITE == 1 and bio:getSubPower() <= 15) -- also erase same tier bio if BIO_OVERWRITE option is on (non-default)
    then -- erase lower tier bio and add dia dot
        target:delStatusEffect(tpz.effect.BIO)
        target:addStatusEffect(tpz.effect.DIA, 2 + dotBonus, 3, duration, 0, 15, 2)
    else 
        spell:setMsg(tpz.msg.basic.MAGIC_NO_EFFECT)
    end

    return final
end
