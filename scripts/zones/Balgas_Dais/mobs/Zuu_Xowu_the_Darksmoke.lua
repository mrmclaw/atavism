-----------------------------------
-- Area: Balga's Dais
--  Mob: Zuu Xowu the Darksmoke - BLM
-- BCNM: Divine Punishers
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = 691, hpp = math.random(45,70)}, -- uses manafont between 45 and 70
        },
    })
end