-----------------------------------
-- Area: Dragons Aery
--  HNM: Fafnir
-----------------------------------
local ID = require("scripts/zones/Dragons_Aery/IDs")
mixins = {require("scripts/mixins/rage")}
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/titles")
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.DRAW_IN, 1)
    mob:setMobMod(tpz.mobMod.DRAW_IN_CUSTOM_RANGE, 20)

    if LandKingSystem_NQ > 0 or LandKingSystem_HQ > 0 then
        GetNPCByID(ID.npc.FAFNIR_QM):setStatus(tpz.status.DISAPPEAR)
    end
    if LandKingSystem_HQ == 0 then
        SetDropRate(805, 3340, 0) -- do not drop cup_of_sweet_tea
    end

    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
    mob:addMod(tpz.mod.STUNRESTRAIT, 80)
    mob:setLocalVar("[rage]timer", 3600) -- 60 minutes
    mob:setMobMod(tpz.mobMod.GIL_MIN, 20000)
    mob:setMobMod(tpz.mobMod.GIL_MAX, 20000)
end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.FAFNIR_SLAYER)
end

function onMobDespawn(mob)
    local kills = GetServerVariable("[PH]Nidhogg") + 1
    local respawn = (75600 + ((math.random(0, 6)) * 1800)) -- 21 - 24 hours with half hour windows
    local spawnChance = 0

    if kills > 2 then
        -- 4th Day = 15%
        -- 5th Day = 27.5%
        -- 6th Day = 40%
        -- 7th Day = 52.5%
        -- 8th Day = 65%
        -- 9th Day = 77.5%
        -- 10th Day = 90%
        -- 11th Day = 100%
        spawnChance = 150 + ((kills - 3) * 125)
    end

    if kills > 2 and (math.random(1000) <= spawnChance) then
        -- Nidhogg
        SetServerVariable("NidhoggUP", 1)
        DisallowRespawn(ID.mob.NIDHOGG, false);
        DisallowRespawn(ID.mob.FAFNIR, true);
        UpdateNMSpawnPoint(ID.mob.NIDHOGG);
        GetMobByID(ID.mob.NIDHOGG):setRespawnTime(respawn)
        SetServerVariable("NidhoggRespawn",(os.time() + respawn))
    else
        -- Fafnir
        DisallowRespawn(ID.mob.FAFNIR, false)
        DisallowRespawn(ID.mob.NIDHOGG, true)
        UpdateNMSpawnPoint(ID.mob.FAFNIR);
        GetMobByID(ID.mob.FAFNIR):setRespawnTime(respawn)
        SetServerVariable("[PH]Nidhogg", kills)
        SetServerVariable("FafnirRespawn",(os.time() + respawn))
    end
end