-----------------------------------
--
-- Zone: Behemoths_Dominion (127)
--
-----------------------------------
local ID = require("scripts/zones/Behemoths_Dominion/IDs")
require("scripts/globals/settings")
require("scripts/globals/zone")
-----------------------------------

function onInitialize(zone)
    local KB = GetServerVariable("KingBehemothUP")    
    local hqbre = GetServerVariable("HQBehemothRespawn")
    local nqbre = GetServerVariable("NQBehemothRespawn")

    if KB == 1 then
        UpdateNMSpawnPoint(ID.mob.KING_BEHEMOTH)
        if os.time() < hqbre then
            GetMobByID(ID.mob.KING_BEHEMOTH):setRespawnTime(hqbre - os.time())
        else
            GetMobByID(ID.mob.KING_BEHEMOTH):setRespawnTime(300) 
        end
    else
        UpdateNMSpawnPoint(ID.mob.BEHEMOTH)
        if os.time() < nqbre then
            GetMobByID(ID.mob.BEHEMOTH):setRespawnTime(nqbre - os.time())
        else
            GetMobByID(ID.mob.BEHEMOTH):setRespawnTime(300) 
        end
    end
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onZoneIn(player, prevZone)
    local cs = -1
    if (player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0) then
        player:setPos(358.134, 24.806, -60.001, 123)
    end
    return cs
end

function onRegionEnter(player, region)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
