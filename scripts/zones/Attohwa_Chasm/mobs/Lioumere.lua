-----------------------------------
-- Area: Attohwa Chasm
--  Mob: Lioumere
--   ID: 16806031
-----------------------------------
mixins = {require("scripts/mixins/families/antlion_ambush")}
require("scripts/globals/keyitems")
require("scripts/globals/missions")
-----------------------------------

function onMobDeath(mob, player, isKiller)
    if (player:getCurrentMission(COP) == tpz.mission.id.cop.THE_ROAD_FORKS and player:getCharVar("MEMORIES_OF_A_MAIDEN_Status") >= 7 and not player:hasKeyItem(tpz.ki.MIMEO_JEWEL)) then
        player:setCharVar("MEMORIES_OF_A_MAIDEN_Status", 8)
        player:setCharVar("LioumereKilled", os.time())
    end
end

function onMobWeaponSkill(player, mob, skill)
    --Sets entire party enmity CE/VE to 0. Need to test for alliance
    mob:pathTo(479.842, 20.00, 40.262)
    local player = player:getParty()
    for _, member in pairs(player) do
        mob:resetEnmity(member)
    end
end

function onMobFight(mob, player, target)
    if mob:atPoint(479.842, 20.00, 40.262) then
        mob:setHP(mob:getMaxHP())
    end

    local playerCE = mob:getCE(player)
    if playerCE >= 180 then
        mob:clearPath()
    end
end
