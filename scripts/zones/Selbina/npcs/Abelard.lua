-----------------------------------
-- Area: Selbina (248)
--  NPC: Abelard
--  An Explorer's Footsteps
-- !pos -52 -11 -13 248 !gotoid 17793039
-- This quest was changed to require a minimum amount of fame to combat RMTs POS-Hacking around to
-- quickly earn gil. However, as this is not a legitimate concern on private servers players may
-- complete this quest even with no fame.
-----------------------------------
local ID = require("scripts/zones/Selbina/IDs")
require("scripts/globals/keyitems")
require("scripts/globals/missions")
require("scripts/globals/npc_util")
require("scripts/globals/settings")
require("scripts/globals/quests")
-----------------------------------

local ZoneID =
{
    0x00001, 800, -- 1 -- West Ronfaure -- !gotoid 17187513
    0x00002, 800, -- 2 -- East Ronfaure -- !gotoid 17191526
    0x00004, 1000, -- 4 -- La Theine Plateau -- !gotoid 17195621
    0x00008, 1000, -- 8 -- Valkurm Dunes -- !gotoid 17199698
    0x00010, 1000, -- 16 -- Jugner Forest -- !gotoid 17203814
    0x00020, 10000, -- 32 -- Batallia Downs -- !gotoid 17207817
    0x00040, 3000, -- 64 -- North Gustaberg -- !gotoid 17212049
    0x00080, 800, -- 128 -- South Gustaberg -- !gotoid 17216174
    0x00100, 1000, -- 256 -- Konschtat Highlands -- !gotoid 17220150
    0x00200, 1000, -- 512 -- Pashhow Marshlands -- !gotoid 17224302
    0x00400, 3000, -- 1024 -- Rolanberry Fields -- !gotoid 17228353
    0x00800, 800, -- 2048 -- West Sarutabaruta -- !gotoid 17248813
    0x01000, 800, -- 4096 -- East Sarutabaruta -- !gotoid 17253058
    0x02000, 1000, -- 8192 -- Tahrongi Canyon -- !gotoid 17257053
    0x04000, 1000, -- 16384 -- Buburimu Peninsula -- !gotoid 17261140
    0x08000, 1000, -- 32768 -- Meriphataud Mountains -- !gotoid 17265242
    0x10000, 10000 -- 65536 -- Sauromugue Champaign -- !gotoid 17269222
}

function tableContains(table, value)  -- function returns true if given table contains given value

    local i = 1
    local contains = false
    
    repeat
        if (table[i] == value) 
        then 
            contains = true
        end
        i = i + 1
    until(i == #table + 1)
    
    return contains
end

function getIndexOf(table, value)  -- function to get index of given value in given array

    local index = 1

    for i = 1, #table, 1 do
        if (table[i] == value) 
        then 
            index = i
            break
        end
    end
    
    return index
end

function onTrade(player, npc, trade)
    if player:getQuestStatus(OTHER_AREAS_LOG, tpz.quest.id.otherAreas.AN_EXPLORER_S_FOOTSTEPS) == QUEST_ACCEPTED and npcUtil.tradeHas(trade, 570) then
        local tablets = player:getCharVar("anExplorer-ClayTablets")
        local currtab = player:getCharVar("anExplorer-CurrentTablet")
        local requestedTablet = player:getCharVar("anExplorer-RequestedTablet")

        if currtab ~= 0 and (tablets % (2 * currtab)) < currtab then -- new tablet
            for zone = 1, #ZoneID, 2 do
                if tablets % (2 * ZoneID[zone]) < ZoneID[zone] then
                    if (tablets + currtab) == 0x1ffff then
                        player:startEvent(47)
                        break
                    end

                    if requestedTablet == currtab then
                        player:startEvent(41) -- the tablet he asked for
                    else
                        player:startEvent(46) -- not the one he asked for
                    end

                    player:setCharVar("anExplorer-ClayTablets", tablets + currtab)
                    break
                end
            end
        end
    end

    if
        player:getCurrentMission(ROV) == tpz.mission.id.rov.SET_FREE and
        npcUtil.tradeHas(trade,{{9082, 3}}) and
        player:getCharVar("RhapsodiesStatus") == 1
    then
        player:startEvent(178)
    end
end

function onTrigger(player, npc)
    local anExplorersFootsteps = player:getQuestStatus(OTHER_AREAS_LOG, tpz.quest.id.otherAreas.AN_EXPLORER_S_FOOTSTEPS)
    local signedInBlood = player:getQuestStatus(SANDORIA, tpz.quest.id.sandoria.SIGNED_IN_BLOOD)
    local signedInBloodStat = player:getCharVar("SIGNED_IN_BLOOD_Prog")
    local rand = math.random(1, 2)  -- used to randomly switch dialogue of Signed in Blood and Exploroers footsteps if both quests are active
    local tabletValues = {}  -- used to store values from stone monuments that player could possibly have visited. later used to find out which places player actually visited
    local turnInAlreadyValues = {} -- used to store values from stone monuments that player actually visited. derived from tabletValues
    local sum = 0 -- accumulation of values of places visited
    local random = math.random(1, #ZoneID)
    local i = 1

    -- SIGNED IN BLOOD
    if signedInBlood == QUEST_ACCEPTED and player:hasKeyItem(tpz.ki.TORN_OUT_PAGES) and signedInBloodStat == 2 then
        player:startEvent(1106) -- FINAL CS FOR SIGNED IN BLOOD FROM ABELARD
    elseif signedInBlood == QUEST_ACCEPTED and signedInBloodStat == 1 then
        player:startEvent(1104)
    elseif signedInBlood == QUEST_ACCEPTED and signedInBloodStat == 2 and anExplorersFootsteps ~= QUEST_AVAILABLE and rand == 1 then
        player:startEvent(1105)
    elseif signedInBlood == QUEST_ACCEPTED and signedInBloodStat == 3 and rand == 1 then
        player:startEvent(1107)

    -- AN EXPLORER'S FOOTSTEPS
    elseif anExplorersFootsteps == QUEST_AVAILABLE and math.floor((player:getFameLevel(SANDORIA) + player:getFameLevel(BASTOK)) / 2) >= 1 then
        player:startEvent(40) -- START EXPLORER'S FOOTSTEPS
    elseif anExplorersFootsteps == QUEST_ACCEPTED then
        if not player:hasItem(570) and not player:hasItem(571) then
            if player:getCharVar("anExplorer-CurrentTablet") == -1 then
                player:startEvent(42)
            else
                player:startEvent(44) -- PLAYER NEEDS CLAY
                player:setCharVar("anExplorer-CurrentTablet", 0)
            end
        else
            local tablets = player:getCharVar("anExplorer-ClayTablets")

            if (tablets == 0) --player hasn't started quest yet
            then 
                player:startEvent(43, 3) --directs player to stone monument in the dunes
            else
            
                while (ZoneID[i] <= tablets)  -- player has turned in at least one tablet. loop through ZoneID table. if value of ZoneID table is less than accumulated total (stored in database) then we possibly visited it already
                do
                    table.insert(tabletValues, ZoneID[i])  -- insert possible places visited inside table that's used in next loop
                    if (i > 32)
                    then
                        break
                    else
                        i = i + 2
                    end
                end
            
                do  -- use the table of places possibly visited to find places actually visited. actually visited places values must add up to value of tablet
                    valuesTabletLength = #tabletValues
                    while sum ~= tablets do  -- once our accumulated total reaches tablets then we have found all places visited by player
                        if not (sum + tabletValues[valuesTabletLength] > tablets) then -- don't want to add values to sum that push us over tablets value
                            table.insert(turnInAlreadyValues, tabletValues[valuesTabletLength])  -- these are places we have turned in stone tablets
                            sum = sum + tabletValues[valuesTabletLength]  
                        end
                        valuesTabletLength = valuesTabletLength - 1
                    end
                end
            
                repeat  -- make new random number until it's a stone monument value (not gil from tha ZoneID table) and isn't a place already visited
                    random = math.random(1, #ZoneID)
                until(random % 2 ~= 0 and not(tableContains(turnInAlreadyValues, ZoneID[random])))
                
                local randomSeek = ZoneID[random]  -- use that random number to get pick place from ZoneID table that player can look for next
                player:setCharVar("anExplorer-RequestedTablet", randomSeek)  -- later used to check if stone tablet player turns in matches one requested by npc
            
                if (getIndexOf(ZoneID, randomSeek) < 18)   -- here we display dialogue of which tablets player has yet to turn in
                then
                    player:startEvent(43, getIndexOf(ZoneID, randomSeek) / 2 - 0.5)
                else
                    player:startEvent(49, getIndexOf(ZoneID, randomSeek) / 2 - 9.5)
                end 
            
            end
    
        end
    else
        player:startEvent(48)
    end
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    -- SIGNED IN BLOOD
    if csid == 1104 then
        player:setCharVar("SIGNED_IN_BLOOD_Prog", 2)
    elseif csid == 1106 then
        player:setCharVar("SIGNED_IN_BLOOD_Prog", 3)

    -- AN EXPLORER'S FOOTSTEPS
    elseif csid == 40 and option ~= 0 and npcUtil.giveItem(player, 571) then
        player:addQuest(OTHER_AREAS_LOG, tpz.quest.id.otherAreas.AN_EXPLORER_S_FOOTSTEPS)
        player:setCharVar("anExplorer-ClayTablets", 0)
        player:setCharVar("anExplorer-RequestedTablet", 8)
    elseif csid == 42 and option == 100 and npcUtil.giveItem(player, 571) then
        player:setCharVar("anExplorer-CurrentTablet", 0)
    elseif csid == 44 then
        npcUtil.giveItem(player, 571)
    elseif csid == 41 or csid == 46 or csid == 47 then
        local currtab = player:getCharVar("anExplorer-CurrentTablet")
        local tablets = player:getCharVar("anExplorer-ClayTablets")

        for zone = 1, #ZoneID, 2 do
            if ZoneID[zone] == currtab then
                player:confirmTrade()
                player:addGil(GIL_RATE * ZoneID[zone+1])
                player:messageSpecial(ID.text.GIL_OBTAINED, GIL_RATE * ZoneID[zone+1])
                player:setCharVar("anExplorer-CurrentTablet", 0)
                break
            end
        end

        if csid == 47 then
            player:completeQuest(OTHER_AREAS_LOG, tpz.quest.id.otherAreas.AN_EXPLORER_S_FOOTSTEPS)
            player:setCharVar("anExplorer-ClayTablets", 0)
        end

        if option == 100 then
            npcUtil.giveItem(player, 571)
        elseif option == 110 then
            player:setCharVar("anExplorer-CurrentTablet", -1)
        end

        if (tablets == 131071 or tablets == 65503 or tablets == 65535 or tablets == 131039) then  -- 131071 is all tablets. 65503 is all except Sauromugue Champaign and Batallia Downs. 65535 is everything except Sauromugue Champaign. 131039 is everyting except Batallia Downs
            npcUtil.giveKeyItem(player, tpz.ki.MAP_OF_THE_CRAWLERS_NEST)
        end

    -- RoV: Set Free
    elseif csid == 178 then
        player:confirmTrade()
        if player:hasJob(0) == 0 then -- Is Subjob Unlocked
            npcUtil.giveKeyItem(player, tpz.ki.GILGAMESHS_INTRODUCTORY_LETTER)
        else
            if not npcUtil.giveItem(player, 8711) then return end
        end
        player:completeMission(ROV, tpz.mission.id.rov.SET_FREE)
        player:addMission(ROV, tpz.mission.id.rov.THE_BEGINNING)
    end
end
