-----------------------------------
-- Area: Jade Sepulcher
--  Mob: Raubahn
-- BLU LB5
-----------------------------------
local ID = require("scripts/zones/Jade_Sepulcher/IDs")
require("scripts/globals/battlefield")
require("scripts/globals/status")
require("scripts/globals/mobs")
-----------------------------------
function onMobInitialize(mob)
	mob:addListener("WEAPONSKILL_STATE_ENTER", "WS_START_MSG", function(mob, wsid)
		if (wsid ~= 2257) and (mob:getLocalVar("Engaged") == 1) then
			mob:showText(mob, ID.text.RAUBAHN_WEAPON_SKILL)
		end
	end)
	mob:addListener("MAGIC_START", "MAGIC_MSG", function(mob, spell, action)
		
		local message = math.random(1,2)
			if message == 1 and (mob:getLocalVar("Engaged") == 1) then
			mob:showText(mob, ID.text.RAUBAHN_CASTING_ONE)
			elseif message == 2 and (mob:getLocalVar("Engaged") == 1) then
			mob:showText(mob, ID.text.RAUBAHN_CASTING_TWO)
			end
		
	end)
	
end

function onMobSpawn(mob)
	mob:setMobMod(tpz.mobMod.MAGIC_COOL, 1)
end

function onMobFight(mob, player, target)
	local bf = mob:getBattlefield()
	if mob:getHPP() <= 60 and (mob:getLocalVar("2HR") == 0) then
		if math.random(1,20) == 20 then
			mob:useMobAbility(2257)
			mob:setLocalVar("2HR", 1)
			mob:showText(mob, ID.text.RAUBAHN_TWO_HOUR)
		end
    end
	if mob:getHPP() <= 20 then
		mob:showText(mob, ID.text.RAUBAHN_DEFEAT)
		bf:setStatus(tpz.battlefield.status.WON)
		mob:setStatus(tpz.status.DISAPPEAR)
    end
end

function onMobEngaged(mob, target)
	mob:setLocalVar("Engaged", 1)
	target:showText(mob, ID.text.RAUBAHN_ENGAGE)
end

function onMobDeath(mob, player, isKiller)
	player:showText(mob, ID.text.RAUBAHN_DEFEAT)
end

function onMobDespawn(mob, player, isKiller)
end
