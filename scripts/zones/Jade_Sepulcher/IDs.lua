-----------------------------------
-- Area: Jade_Sepulcher
-----------------------------------
require("scripts/globals/zone")
-----------------------------------

zones = zones or {}

zones[tpz.zone.JADE_SEPULCHER] =
{
    text =
    {
        ITEM_CANNOT_BE_OBTAINED = 6382, -- You cannot obtain the <item>. Come back after sorting your inventory.
        ITEM_OBTAINED           = 6388, -- Obtained: <item>.
        GIL_OBTAINED            = 6389, -- Obtained <number> gil.
        KEYITEM_OBTAINED        = 6391, -- Obtained key item: <keyitem>.
        CARRIED_OVER_POINTS     = 6999, -- You have carried over <number> login point[/s].
        LOGIN_CAMPAIGN_UNDERWAY = 7000, -- The [/January/February/March/April/May/June/July/August/September/October/November/December] <number> Login Campaign is currently underway!<space>
        LOGIN_NUMBER            = 7001, -- In celebration of your most recent login (login no. <number>), we have provided you with <number> points! You currently have a total of <number> points.
		CANNOT_ENTER_LEVEL_RESTRICTED = 7021, -- Your party is unable to participate because certain members' levels are restricted.
        ORB_MESSAGE_OFFSET      = 7623, -- There is a crack in the <item>. It no longer contains a monster.
		RAUBAHN_ENGAGE			= 7699, -- Come, surrender your strength to me and serve the Empire in death!
		RAUBAHN_DEFEAT			= 7730, -- My strength has failed me...
		RAUBAHN_CASTING_ONE		= 7733, -- You will be buried in the shadow of forgotten history!
		RAUBAHN_CASTING_TWO		= 7732, -- Show me! Show me the power that lurks within you!
		RAUBAHN_WEAPON_SKILL    = 7731, -- Blue magic is but one part of our arsenal. A fact you will soon learn...
		RAUBAHN_TWO_HOUR		= 7734, -- Now you will bear the full torrent of my azure savagery!
		PLAYER_TWO_HOUR			= 7736, -- Hahaha... The greater the power you wield, the greater my strength will be when I devour your essence...!
    },
    mob =
    {
        LANCELORD_GAHEEL_JA = 17051694,
    },
    npc =
    {
    },
}

return zones[tpz.zone.JADE_SEPULCHER]
