-----------------------------------
-- Area: Talacca Cove
-- BCNM: COR LB5: Breaking the Bonds of Fate
-----------------------------------
require("scripts/globals/battlefield")
require("scripts/globals/missions")
require("scripts/globals/titles")
require("scripts/globals/quests")
require("scripts/globals/utils")
----------------------------------------

function onBattlefieldTick(battlefield, tick)
    tpz.battlefield.onBattlefieldTick(battlefield, tick)
end

function onBattlefieldRegister(player, battlefield)
end

function onBattlefieldEnter(player, battlefield)
end

function onBattlefieldLeave(player, battlefield, leavecode)
    if leavecode == tpz.battlefield.leaveCode.WON then -- play end CS. Need time and battle id for record keeping + storage
        local name, clearTime, partySize = battlefield:getRecord()
        player:startEvent(32001, battlefield:getArea(), clearTime, partySize, battlefield:getTimeInside(), 1, battlefield:getLocalVar("[cs]bit"), 0)
    elseif leavecode == tpz.battlefield.leaveCode.LOST then
        player:startEvent(32002)
    end
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
    local bonds = player:getQuestStatus(AHT_URHGAN, tpz.quest.id.ahtUrhgan.BREAKING_THE_BONDS_OF_FATE)

    if csid == 32001 and bonds == QUEST_ACCEPTED then
        player:levelCap(75)
        player:showText(player, 7851)
        npcUtil.giveItem(player, 4181)
        npcUtil.completeQuest(player, AHT_URHGAN, tpz.quest.id.ahtUrhgan.BREAKING_THE_BONDS_OF_FATE, {
            title = tpz.title.MASTER_OF_CHANCE,
        })
    end
end
