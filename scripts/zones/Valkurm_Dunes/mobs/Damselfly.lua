-----------------------------------
-- Area: Valkurm Dunes
--  Mob: Damselfly
-- Note: Place holder Valkurm Emperor
-----------------------------------
local ID = require("scripts/zones/Valkurm_Dunes/IDs")
require("scripts/globals/regimes")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob)
    local VEre = GetServerVariable("VErespawn")
    local count = GetServerVariable("VEcount")
    if os.time() > VEre then
        if
            (mob:getID() == 17199434) or
            (mob:getID() == 17199435) or
            (mob:getID() == 17199436) or
            (mob:getID() == 17199437) or
            (mob:getID() == 17199419) or
            (mob:getID() == 17199420) then

            count = count + 1
            if count >= 100 then
                tpz.mob.phOnDespawn(mob, ID.mob.VALKURM_EMPEROR_PH, 5, 3600) -- 1 hour
            elseif count >= 75 then
                tpz.mob.phOnDespawn(mob, ID.mob.VALKURM_EMPEROR_PH, 4, 3600) -- 1 hour
            elseif count >= 50 then
                tpz.mob.phOnDespawn(mob, ID.mob.VALKURM_EMPEROR_PH, 3, 3600) -- 1 hour
            else
                tpz.mob.phOnDespawn(mob, ID.mob.VALKURM_EMPEROR_PH, 2, 3600) -- 1 hour
            end
            SetServerVariable("VEcount", count)
        end
     end
end


function onMobDespawn(mob)

end
