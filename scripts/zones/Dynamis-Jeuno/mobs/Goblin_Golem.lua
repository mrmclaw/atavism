-----------------------------------
-- Area: Dynamis - Jeuno
--  Mob: Goblin Golem
-- Note: Mega Boss
-----------------------------------
require("scripts/globals/dynamis")
require("scripts/globals/status")
require("scripts/zones/Dynamis-Jeuno/dynamis_mobs")
local ID = require("scripts/zones/Dynamis-Jeuno/IDs")
-----------------------------------

local zone = 188

function onMobSpawn(mob)
    local mobID = mob:getID()
    dynamis.statueOnSpawn(mob, mobList[zone][mobID] ~= nil and mobList[zone][mobID].eyes or 0)
    dynamis.setMegaBossStats(mob)
end

function onMobDeath(mob, player, isKiller)
    dynamis.statueOnDeath(mob, player, isKiller)
    dynamis.mobOnDeath(mob, mobList[zone], ID.text.DYNAMIS_TIME_EXTEND)
    local winQM = GetNPCByID(17547509)
    local pos = mob:getPos()
    winQM:setPos(pos.x,pos.y,pos.z,pos.rot)
    winQM:setStatus(tpz.status.NORMAL)
    player:addTitle(tpz.title.DYNAMIS_JEUNO_INTERLOPER)
end

function onMobRoamAction(mob)
    dynamis.mobOnRoamAction(mob)
end

function onMobRoam(mob)
    dynamis.mobOnRoam(mob)
end

function onMobEngaged(mob, target)
    randomChildrenCount = mobList[zone][mob:getID()].randomChildrenCount
    randomChildrenListArg = {}
    while randomChildrenCount > 0 do
        if mobList[zone][mob:getID()].randomChildrenList ~= nil then
            randomChildrenListArg[randomChildrenCount] = randomChildrenList[zone][mobList[zone][mob:getID()].randomChildrenList[randomChildrenCount]]
        end
        randomChildrenCount = randomChildrenCount - 1
    end
    dynamis.statueOnEngaged(mob, target, mobList[zone], randomChildrenListArg)
end
