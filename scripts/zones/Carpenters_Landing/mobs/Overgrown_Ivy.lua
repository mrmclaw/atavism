-----------------------------------
-- Area: Carpenters_Landing
--  Mob: Overgrown Ivy
-----------------------------------
function onMobInitialize(mob)
    mob:addMod(tpz.mod.DOUBLE_ATTACK, 10) -- added a double attack at 10% chance
    mob:addMod(tpz.mod.REGAIN, 50)
end

function onMobDeath(mob, player, isKiller)
    if (player:getCurrentMission(COP) == tpz.mission.id.cop.THE_ROAD_FORKS and player:getCharVar("EMERALD_WATERS_Status") == 4) then
        player:setCharVar("EMERALD_WATERS_Status", 5)
    end
end

function onMobWeaponSkillPrepare(mob, target)
    local roll = math.random()
    if mob:getHPP() <= 15 then
        return 319
    end
end

function onMobFight(mob, target)
    if mob:getHPP() <= 15 then
        print(mob:getTP())
        mob:setMod(tpz.mod.REGAIN, 200)
    end
end