-----------------------------------
-- Area: Maze of Shakhrami
--  NPC: Excavation Point
-- Used in Quest: The Holy Crest
-- !pos 234 0.1 -110 198
-----------------------------------
require("scripts/globals/npc_util")
require("scripts/globals/helm")
local ID = require("scripts/zones/Maze_of_Shakhrami/IDs")
-----------------------------------

function onTrade(player, npc, trade)
    if (player:getFreeSlotsCount() == 0)
	and not player:hasItem(1159) then
        player:messageSpecial(ID.text.ITEM_CANNOT_BE_OBTAINED, 1159)
	  elseif player:getCharVar("TheHolyCrest_Event") == 3 
		  and not player:hasItem(1159) 
		  and npcUtil.tradeHas(trade, 605) 
		  and (player:getFreeSlotsCount() >= 1) then
		player:startEvent(60, 1159, 0, 0, 0, 4294966560)
		player:addItem(1159)
    else
        tpz.helm.onTrade(player, npc, trade, tpz.helm.type.EXCAVATION, 60)
    end

end

function onTrigger(player, npc)
    tpz.helm.onTrigger(player, tpz.helm.type.EXCAVATION)
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
