-----------------------------------
-- Area: Rolanberry Fields
--  Mob: Silk Caterpillar
-----------------------------

function onMobSpawn(mob)
    mob:setLocalVar("despawn", os.time() + 210)
end

function onMobRoam(mob)
    local now = os.time()
    local despawntime = mob:getLocalVar("despawn")
    if now > despawntime then
        DespawnMob(mob:getID())
    end
end