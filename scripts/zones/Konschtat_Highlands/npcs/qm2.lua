-----------------------------------
-- Area: Konschtat Highlands
--  NPC: qm2 (???)
-- Involved in Quest: Forge Your Destiny
-- !pos -709 2 102 108
-----------------------------------
local ID = require("scripts/zones/Konschtat_Highlands/IDs")
require("scripts/globals/npc_util")
require("scripts/globals/quests")
-----------------------------------

function onTrade(player, npc, trade)
    if
        player:getQuestStatus(OUTLANDS, tpz.quest.id.outlands.FORGE_YOUR_DESTINY) == QUEST_ACCEPTED and
        npcUtil.tradeHas(trade, 1151) and player:checkDistance(npc) > 3
	then
		player:messageSpecial(ID.text.BLACKENED_SPOT_5)
	elseif
        player:getQuestStatus(OUTLANDS, tpz.quest.id.outlands.FORGE_YOUR_DESTINY) == QUEST_ACCEPTED and
        npcUtil.tradeHas(trade, 1151) and player:checkDistance(npc) < 3 and (os.time() < npc:getLocalVar("cooldown")) or
		GetMobByID(ID.mob.FORGER):isSpawned()		
    then
		player:messageSpecial(ID.text.BLACKENED_SPOT_4, 1151)
	elseif
        player:getQuestStatus(OUTLANDS, tpz.quest.id.outlands.FORGE_YOUR_DESTINY) == QUEST_ACCEPTED and
        npcUtil.tradeHas(trade, 1151) and player:checkDistance(npc) < 3 and (os.time() >= npc:getLocalVar("cooldown")) and
        not GetMobByID(ID.mob.FORGER):isSpawned()
    then
        SpawnMob(ID.mob.FORGER):updateClaim(player)
        player:confirmTrade()
		player:messageSpecial(ID.text.BLACKENED_SPOT_3, 1151)
    end
end

function onTrigger(player, npc)
	if
	player:getQuestStatus(OUTLANDS, tpz.quest.id.outlands.FORGE_YOUR_DESTINY) == QUEST_ACCEPTED and player:hasItem(1151)
	then
	player:messageSpecial(ID.text.BLACKENED_SPOT_2, 1151)
	else
	player:messageSpecial(ID.text.BLACKENED_SPOT_1)
	end
end

function onEventUpdate(player, csid, option)
end

function onEventFinish(player, csid, option)
end
