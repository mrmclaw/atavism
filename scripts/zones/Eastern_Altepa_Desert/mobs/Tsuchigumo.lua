-----------------------------------
-- Area: Eastern Altepa Desert
--  Mob: Tsuchigumo
-- Involved in Quest: 20 in Pirate Years
-----------------------------------

function onMobSpawn(mob)
    mob:setLocalVar("despawnTime", os.time() + 300)
end

function onMobRoam(mob)
    local despawnTime = mob:getLocalVar("despawnTime")
        if despawnTime > 0 and os.time() > despawnTime then
        DespawnMob(mob:getID())
        end
end


function onMobDeath(mob, player, isKiller)
    if player:getCharVar("twentyInPirateYearsCS") == 3 then
        player:addCharVar("TsuchigumoKilled", 1)
    end
end
