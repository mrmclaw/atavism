-----------------------------------
-- Area: The Shrine of Ru'Avitau
--  Mob: Aura Statue
-----------------------------------
require("scripts/globals/regimes")
local ID = require("scripts/zones/The_Shrine_of_RuAvitau/IDs")
-----------------------------------

function onMobDespawn(mob, target)
	tpz.mob.phOnDespawn(mob, ID.mob.ULLIKUMMI_PH, 5, 10800) -- 3 hours
end

function onMobDeath(mob, player, isKiller)
    tpz.regime.checkRegime(player, mob, 749, 1, tpz.regime.type.GROUNDS)
    tpz.regime.checkRegime(player, mob, 754, 1, tpz.regime.type.GROUNDS)
end
