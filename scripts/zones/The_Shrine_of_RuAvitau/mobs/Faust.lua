-----------------------------------
-- Area: The Shrine of Ru'Avitau
--  Mob: Faust
-- Hallway 1:: 739.973, 0.400, -99.555, 1
-- Hallway 2:: 739.973, 0.400, -99.555, 193
-----------------------------------
function onMobSpawn(mob)
end

function onMobRoam(mob)
	local facing = math.random(1, 10)
	if (mob:getXPos() > 740 or mob:getXPos() < 739) or (mob:getYPos() > 0.45 or mob:getYPos() < .35) or (mob:getZPos() > -99 or mob:getZPos() < -100) then
		mob:pathTo(739.973, 0.400, -99.555, 1)
	end 
	
	if facing < 6 and os.time () > (mob:getLocalVar("Rotated") + 7) then
		mob:setPos(mob:getXPos(), mob:getYPos(), mob:getZPos(), 1)
		mob:setLocalVar("Rotated", os.time())
	end
	
	if facing > 5 and os.time () > (mob:getLocalVar("Rotated") + 7) then
		mob:setPos(mob:getXPos(), mob:getYPos(), mob:getZPos(), 193)
		mob:setLocalVar("Rotated", os.time())
	end
		
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
	UpdateNMSpawnPoint(mob:getID())
	local respawn = math.random(10800, 21600) -- 3 to 6 hours
	mob:setRespawnTime(respawn)
	SetServerVariable("FaustRespawn",(os.time() + respawn))
	mob:setTP(0)
end


function onMobFight(mob, target)
	local changeTime = mob:getLocalVar("changeTime")
	local state = mob:getLocalVar("state")
	-- > 40% uses Typhoon back to back every 15 seconds
	if mob:getHPP() >= 40 and mob:getBattleTime() - changeTime >= 15 then
		print("wrong")
		mob:useMobAbility(539)
		mob:timer(4500, function(faust)
			if faust:checkDistance(faust:getTarget()) <= 6 then
				faust:useMobAbility(539)
			end
		end)
		mob:setLocalVar("changeTime", mob:getBattleTime())
		-- < 40% spams Typhoon over and over
	elseif mob:getHPP() < 40 and state == 0 then
		print("right")
		mob:setMobMod(tpz.mobMod.SKILL_LIST, 367)
		mob:setMod(tpz.mod.REGAIN, 3000)
		mob:setLocalVar("state", 1)
	end
end

function onMobDisengage(mob)
	mob:setLocalVar("changeTime", 0)
	mob:setLocalVar("state", 0)
	mob:setTP(0)
end