-----------------------------------
-- Area: Halvung
-- Mob: Friar's Lantern
-- Note: Place holder Big Bomb
-----------------------------------
local ID = require("scripts/zones/Halvung/IDs")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob)
    tpz.mob.phOnDespawn(mob, ID.mob.BIG_BOMB, 2, math.random(64800,86400)) -- 18-24 hours
end