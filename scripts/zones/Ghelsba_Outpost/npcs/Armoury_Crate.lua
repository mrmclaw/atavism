-----------------------------------
-- Area: Ghelsba_Outpost
-- NPC:  Armoury Crate
-- Ghelsba_Outpost Armoury_Crate
-------------------------------------

require("scripts/globals/bcnm")
require("scripts/globals/battlefield")
-----------------------------------
local loot =
{
    -- BCNM Wings of Fury
    [34] =
    {
        {
            {itemid = 891, droprate = 1000}, -- Bat Fang
        },
        {
            {itemid = 4900, droprate = 300}, -- Thunder Spirit Pact
            {itemid = 4744, droprate = 300}, -- Scroll Of Invisible
            {itemid = 4745, droprate = 150}, -- Scroll Of Sneak
            {itemid = 4746, droprate = 250}, -- Scroll Of Deodorize
        },
        {
            {itemid = 17786, droprate = 153},-- Ganko
            {itemid = 18170, droprate = 139},-- Platoon Edge
            {itemid = 16687, droprate = 83}, -- Platoon Axe
            {itemid = 17571, droprate = 95}, -- Platoon Pole
            {itemid = 17993, droprate = 125},-- Platoon Dagger
            {itemid = 749, droprate = 350},  -- Mythril Beastcoin
            {itemid = 773, droprate = 55},   -- Translucent Rock
        },
        {
            {itemid = 17820, droprate = 125},-- Gunromaru
            {itemid = 17993, droprate = 150},-- Platoon Dagger
            {itemid = 18170, droprate = 150},-- Platoon Edge
            {itemid = 18085, droprate = 50}, -- Platoon Lance
            {itemid = 16959, droprate = 200},-- Platoon Sword
            {itemid = 774, droprate = 100},   -- Purple Rock
            {itemid = 769, droprate = 75},   -- Red Rock
            {itemid = 776, droprate = 14},   -- White Rock
            {itemid = 775, droprate = 28},   -- Black Rock
            {itemid = 772, droprate = 28},   -- Green Rock
            {itemid = 771, droprate = 15},   -- Yellow Rock
            {itemid = 770, droprate = 65},   -- Blue Rock
        },
        {
            {itemid =     0, droprate = 389}, -- nothing
            {itemid = 13548, droprate = 167}, -- astral_ring
            {itemid =   922, droprate = 444}, -- bat_wing
        },
    },

    -- BCNM Petrifying Pair
    [35] =
    {
        {
            {itemid = 852, droprate = 1000}, -- Lizard Skin
        },
        {
            {itemid =     0, droprate =  900}, -- nothing
            {itemid = 15351, droprate =  100}, -- bounding_boots
        },
        {
            {itemid = 15282, droprate =  100}, -- Katana Obi
            {itemid = 15275, droprate =  100}, -- Rapier Belt
            {itemid = 15278, droprate =  175}, -- Scythe Belt
            {itemid =   694, droprate =  175}, -- Chestnut Log
            {itemid =   690, droprate =  350}, -- Elm Log
            {itemid =   652, droprate =  100}, -- Steel Ingot
        },
        {
            {itemid =     0, droprate =  500}, -- nothing (50%)
            {itemid = 15282, droprate =   50}, -- Katana Obi
            {itemid = 15275, droprate =   50}, -- Rapier Belt
            {itemid = 15278, droprate =   87}, -- Scythe Belt
            {itemid =   694, droprate =   88}, -- Chestnut Log
            {itemid =   690, droprate =  175}, -- Elm Log
            {itemid =   652, droprate =   50}, -- Steel Ingot
        },
        {
            {itemid = 15285, droprate = 150},-- Avatar Belt
            {itemid = 15279, droprate = 150},-- Pick Belt
            {itemid = 651, droprate =   150},  -- Iron Ingot
            {itemid = 643, droprate =   150},  -- Chunk Of Iron Ore
            {itemid = 644, droprate =   125},   -- Chunk Of Mythril Ore
            {itemid = 736, droprate =   125},   -- Chunk Of Silver Ore
            {itemid = 795, droprate =   150},  -- Lapis Lazuli
        },
        {
            {itemid = 17867, droprate = 225},-- Jug Of Cold Carrion Broth
            {itemid = 4877, droprate =  150}, -- Scroll Of Absorb-agi
            {itemid = 4878, droprate =  125}, -- Scroll Of Absorb-int
            {itemid = 4876, droprate =  150}, -- Scroll Of Absorb-vit
            {itemid = 4868, droprate =   50}, -- Scroll Of Dispel
            {itemid = 4751, droprate =   50},  -- Scroll Of Erase
            {itemid = 5070, droprate =  200}, -- Scroll Of Magic Finale
            {itemid = 4947, droprate =   50},  -- Scroll Of Utsusemi Ni
        },
        {
            {itemid =     0, droprate =  250}, -- nothing (25%)
            {itemid = 15271, droprate =   50}, -- Axe Belt
            {itemid = 15272, droprate =   40}, -- Cestus Belt
            {itemid =   809, droprate =   10}, -- Clear Topaz
            {itemid = 15276, droprate =   50}, -- Dagger Belt
            {itemid =   645, droprate =   50}, -- Darksteel Ore
            {itemid = 15281, droprate =   25}, -- Gun Belt
            {itemid =  4132, droprate =   50}, -- Hi-ether
            {itemid = 15284, droprate =   50}, -- Lance Belt
            {itemid =   796, droprate =   50}, -- Light Opal
            {itemid = 15273, droprate =   50}, -- Mace Belt
            {itemid =   653, droprate =   50}, -- Mythril Ingot
            {itemid =   799, droprate =   25}, -- Onyx
            {itemid = 15283, droprate =   50}, -- Sarashi
            {itemid = 15277, droprate =   50}, -- Shield Belt
            {itemid = 15280, droprate =   25}, -- Song Belt
            {itemid = 15274, droprate =   25}, -- Staff Belt
            {itemid =   744, droprate =   50}, -- Silver Ingot
            {itemid =   806, droprate =   50}, -- Tourmaline
        },
        {
            {itemid =     0, droprate =  250}, -- nothing (25%)
            {itemid = 15271, droprate =   50}, -- Axe Belt
            {itemid = 15272, droprate =   40}, -- Cestus Belt
            {itemid =   809, droprate =   10}, -- Clear Topaz
            {itemid = 15276, droprate =   50}, -- Dagger Belt
            {itemid =   645, droprate =   50}, -- Darksteel Ore
            {itemid = 15281, droprate =   25}, -- Gun Belt
            {itemid =  4132, droprate =   50}, -- Hi-ether
            {itemid = 15284, droprate =   50}, -- Lance Belt
            {itemid =   796, droprate =   50}, -- Light Opal
            {itemid = 15273, droprate =   50}, -- Mace Belt
            {itemid =   653, droprate =   50}, -- Mythril Ingot
            {itemid =   799, droprate =   25}, -- Onyx
            {itemid = 15283, droprate =   50}, -- Sarashi
            {itemid = 15277, droprate =   50}, -- Shield Belt
            {itemid = 15280, droprate =   25}, -- Song Belt
            {itemid = 15274, droprate =   25}, -- Staff Belt
            {itemid =   744, droprate =   50}, -- Silver Ingot
            {itemid =   806, droprate =   50}, -- Tourmaline
        },
    },

    -- BCNM Toadal Recall
    [36] =
    {
        {
            {itemid =  4373, droprate = 1000}, -- woozyshroom
        },
        {
            {itemid = 65535, droprate = 1000, amount = 4000}, -- gil
        },
        {
            {itemid =     0, droprate =  200}, -- nothing
            {itemid = 12403, droprate =  150}, -- magicians_shield
            {itemid = 12389, droprate =  150}, -- mercenarys_targe
            {itemid = 12399, droprate =  150}, -- beaters_aspis
            {itemid = 12394, droprate =  150}, -- pilferers_aspis
            {itemid =   806, droprate =   70}, -- tourmaline
            {itemid =   814, droprate =   70}, -- amber
            {itemid =   795, droprate =   60}, -- lapis_lazuli
        },
        {
            {itemid =     0, droprate =  250}, -- nothing
            {itemid = 13667, droprate =  250}, -- trimmers_mantle
            {itemid = 13671, droprate =  250}, -- genin_mantle
            {itemid = 13663, droprate =  250}, -- warlocks_mantle
        },
        {
            {itemid =     0, droprate =  325}, -- nothing
            {itemid =  4947, droprate =  100}, -- scroll_of_utsusemi_ni
            {itemid =  4714, droprate =  125}, -- scroll_of_phalanx
            {itemid =  4751, droprate =  125}, -- scroll_of_erase
            {itemid = 17880, droprate =  150}, -- jug_of_seedbed_soil
            {itemid =   645, droprate =   50}, -- darksteel_ore
            {itemid =  4374, droprate =  150}, -- sleepshroom
        },
        {
            {itemid =     0, droprate =  150}, -- nothing
            {itemid =  1601, droprate =  200}, -- mannequin_head
            {itemid =  1602, droprate =  200}, -- mannequin_body
            {itemid =  1603, droprate =  200}, -- mannequin_hands
            {itemid =  4868, droprate =  150}, -- scroll_of_dispel
            {itemid =  4386, droprate =  100}, -- king_truffle
        },
    },
}

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local battlefield = player:getBattlefield()
    if battlefield then
        tpz.battlefield.HandleLootRolls(battlefield, loot[battlefield:getID()], nil, npc)
    end
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
