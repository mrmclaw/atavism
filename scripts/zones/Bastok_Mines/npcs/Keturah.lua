-----------------------------------
-- Area: Port Bastok
--
--
--
-----------------------------------
local ID = require("scripts/zones/Port_Bastok/IDs")
-----------------------------------

function onTrigger(player, npc)
local LvL = player:getMainLvl()

    if player:getCharVar("AnnyRing") == 0 and LvL >= 10 and (player:getFreeSlotsCount() >= 1) then
        player:addItem(15793)
        player:messageSpecial(ID.text.ITEM_OBTAINED, 15793)
        player:setCharVar("AnnyRing", 1)
        player:PrintToPlayer("Congratulations on level 10, here is your reward!",29)
    elseif player:getCharVar("AnnyRing") == 1 then
        player:PrintToPlayer("You already claimed the release campaign reward.",29)
    elseif  player:getCharVar("AnnyRing") == 0 and LvL >= 10 and (player:getFreeSlotsCount() == 0) then
    player:messageSpecial(ID.text.ITEM_CANNOT_BE_OBTAINED, 15793)
    else
        player:PrintToPlayer("You need to reach level 10 to claim the release campaign reward.",29)
    end
end

