-----------------------------------
-- Area: Upper Delkfutt's Tower
--   NM: Alkyoneus
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobSpawn(mob)
    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = tpz.jsa.MIGHTY_STRIKES},
        },
    })
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
end


function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    UpdateNMSpawnPoint(mob:getID())
    local respawn = math.random(75600,86400) -- 21-24 hours
    mob:setRespawnTime(respawn)
    SetServerVariable("AlkyoneusRespawn",(os.time() + respawn))
end