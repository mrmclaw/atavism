-----------------------------------
-- Area: Upper Delkfutt's Tower
--   NM: Pallas
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    UpdateNMSpawnPoint(mob:getID())
    local respawn = math.random(64800,86400) -- 18-24 hours
    mob:setRespawnTime(respawn)
    SetServerVariable("PallasRespawn",(os.time() + respawn))
end