-----------------------------------
-- Area: Cape Teriggan
--   NM: Kreutzet
-----------------------------------
require("scripts/globals/world")
require("scripts/globals/mobs")
local ID = require("scripts/zones/Cape_Teriggan/IDs")
-----------------------------------

function onMobSpawn(mob)
    mob:setMobMod(tpz.mobMod.CLAIM_SHIELD, 1)
end

function onMobRoam(mob)
    if not (mob:getWeather() == tpz.weather.WIND or mob:getWeather() == tpz.weather.GALES) then
        DespawnMob(mob:getID())
    end
end

function onMobWeaponSkill(target, mob, skill)
    if skill:getID() == 926 then
        local stormwindCounter = mob:getLocalVar("stormwindCounter")

        stormwindCounter = stormwindCounter +1
        mob:setLocalVar("stormwindCounter", stormwindCounter)
        mob:setLocalVar("stormwindDamage", stormwindCounter) -- extra var for dmg calculation (in stormwind.lua)

        if stormwindCounter > 2 then
            mob:setLocalVar("stormwindCounter", 0)
        else
            mob:useMobAbility(926)
        end
    end
end

function onMobDisengage(mob, weather)
    if not (mob:getWeather() == tpz.weather.WIND or mob:getWeather() == tpz.weather.GALES) then
        DespawnMob(mob:getID())
    end
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    UpdateNMSpawnPoint(mob:getID())
    local Kreutzet = GetMobByID(ID.mob.KREUTZET)
    local respawnmin = 32400 -- 9h base
    local respawnmax = 43200 -- 12h max
    mob:setRespawnTime(respawnmin)
    SetServerVariable("KruetzetRespawn",(os.time() + respawnmin))
    SetServerVariable("KruetzetRespawnMax",(os.time() + respawnmax))
    DisallowRespawn(Kreutzet:getID(), true)
end