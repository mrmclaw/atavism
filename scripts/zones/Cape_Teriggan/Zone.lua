-----------------------------------
--
-- Zone: Cape_Teriggan (113)
--
-----------------------------------
local ID = require("scripts/zones/Cape_Teriggan/IDs")
require("scripts/quests/i_can_hear_a_rainbow")
require("scripts/globals/conquest")
require("scripts/globals/world")
require("scripts/globals/zone")
-----------------------------------

function onInitialize(zone)
    UpdateNMSpawnPoint(ID.mob.KREUTZET)
    local Kreutzet = GetMobByID(ID.mob.KREUTZET)
    local krre = GetServerVariable("KruetzetRespawn")
    DisallowRespawn(Kreutzet:getID(), true)

    if os.time() < krre then
        GetMobByID(ID.mob.KREUTZET):setRespawnTime(krre - os.time())
    end

    tpz.conq.setRegionalConquestOverseers(zone:getRegionID())
end

function onConquestUpdate(zone, updatetype)
    tpz.conq.onConquestUpdate(zone, updatetype)
end

function onZoneIn( player, prevZone)
    local cs = -1

    if player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0 then
        player:setPos( 315.644, -1.517, -60.633, 108)
    end

    if quests.rainbow.onZoneIn(player) then
        cs = 2
    end

    return cs
end

function onRegionEnter( player, region)
end

function onEventUpdate( player, csid, option)
    if csid == 2 then
        quests.rainbow.onEventUpdate(player)
    end
end

function onEventFinish( player, csid, option)
end

function onZoneWeatherChange(weather)
    UpdateNMSpawnPoint(ID.mob.KREUTZET)
    local krre = GetServerVariable("KruetzetRespawn")
    local krremax = GetServerVariable("KruetzetRespawnMax")
    local Kreutzet = GetMobByID(ID.mob.KREUTZET)

    if
    not Kreutzet:isSpawned() and os.time() > krremax
            and (weather == tpz.weather.WIND or weather == tpz.weather.GALES)
    then
        DisallowRespawn(Kreutzet:getID(), false)
        SpawnMob(ID.mob.KREUTZET)
    elseif
    not Kreutzet:isSpawned() and os.time() > krre
            and (weather == tpz.weather.WIND or weather == tpz.weather.GALES)
    then
        -- 20% chance to pop
        local chance = math.random(1, 5)
        if chance == 1 then
            DisallowRespawn(Kreutzet:getID(), false)
            SpawnMob(ID.mob.KREUTZET)
        end
    end
end