-----------------------------------
-- Area: Toraimarai Canal
--   NM: Magic Sludge
-----------------------------------

function onMobInitialize(mob)
    mob:addMod(tpz.mod.UDMGPHYS, -75)
    mob:addMod(tpz.mod.UDMGRANGE, -75)
    mob:addMod(tpz.mod.UDMGMAGIC, -75)
	mob:addMod(tpz.mod.UDMGBREATH, -75)
end

function onMobSpawn(mob)

end

function onMobSpawn(mob)
    mob:setMod(tpz.mod.UDMGMAGIC, -75)
end

function onMobDeath(mob, player, isKiller)
    player:setCharVar("rootProblem", 3)

end
