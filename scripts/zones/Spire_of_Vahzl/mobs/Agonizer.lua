-----------------------------------
-- Area: Spire of Vahzl
--  Mob: Agonizer
-----------------------------------
require("scripts/globals/pathfind")
-----------------------------------

function onMobInitialize(mob)
end

function onMobSpawn(mob)
	mob:setMobMod(tpz.mobMod.NO_MOVE, 1)
	mob:setMobMod(tpz.mobMod.NO_AGGRO, 1)
	mob:setMobMod(tpz.mobMod.NO_LINK, 1)
	mob:setMod(tpz.mod.DOUBLE_ATTACK, 20)
	mob:delRoamFlag(512)
end

function onMobEngaged(mob, target)
	mob:setMobMod(tpz.mobMod.NO_MOVE, 0)
	mob:setMobMod(tpz.mobMod.NO_AGGRO, 0)
end

function onMobWeaponSkill(target, mob, skill)
end

function onMobFight(mob, target)
	if mob:getTP() >= 2000 then
		mob:useMobAbility()
	end

	if mob:getHPP() < 20 then
		local nextMob = GetMobByID(mob:getID() + 6) --Cumulator aggros at <20%
		if not nextMob:isEngaged() then
			nextMob:updateEnmity(target)
			nextMob:setMobMod(tpz.mobMod.NO_MOVE, 0)
			nextMob:setMobMod(tpz.mobMod.NO_AGGRO, 0)
		end
	end

	local terrorEndTime = mob:getLocalVar("EmptyTerror")
	if terrorEndTime == 0 then
		return
	elseif terrorEndTime < os.time() then
		mob:setLocalVar("EmptyTerror",0)
		mob:delRoamFlag(512)
		return
	end

	-- scripted run around
	mob:addRoamFlag(512) -- ignore attacking
	if not mob:isFollowingPath() then
		mob:disengage()
		local point = {math.random(-249,-230),60.9,math.random(-8,10)}
		mob:pathThrough(point, tpz.path.flag.RUN)
	end

	if mob:getHPP() <= 35 then
		mob:setMod(tpz.mod.STORETP, 250)
	end
end

function onMobWeaponSkillPrepare(mob, target)
	local roll = math.random()
	if mob:getHPP() <= 25 then
		if roll <= 0.25 then
			return 1252 -- shadow_spread
		else return 1248 -- trinary_absorption
		end
	end
end

function onMobRoam(mob)
	local terrorEndTime = mob:getLocalVar("EmptyTerror")
	if terrorEndTime == 0 then
		return
	elseif terrorEndTime < os.time() then
		mob:setLocalVar("EmptyTerror",0)
		mob:delRoamFlag(512)
		return
	end

	-- scripted run around
	mob:addRoamFlag(512) -- ignore attacking
	if not mob:isFollowingPath() then
		mob:disengage()
		local point = {math.random(-249,-230),60.9,math.random(-8,10)}
		mob:pathThrough(point, tpz.path.flag.RUN)
	end

end

function onMobDeath(mob, player, isKiller)
end
