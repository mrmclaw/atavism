-----------------------------------
-- Area: Castle Oztroja (151)
--  Mob: Yagudo Oracle
-- Note: PH for Quu Domi the Gallant
-----------------------------------
local ID = require("scripts/zones/Castle_Oztroja/IDs")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDeath(mob)
    local QUUre = GetServerVariable("QUUrespawn")
    local count = GetServerVariable("QUUcount")
    if os.time() > QUUre then
        if
            (mob:getID() == 17395845) or
            (mob:getID() == 17395853) or
            (mob:getID() == 17395831) or
            (mob:getID() == 17395868) then

            count = count + 1
            if count >= 60 then
                tpz.mob.phOnDespawn(mob, ID.mob.QUU_DOMI_THE_GALLANT_PH, 5, 3000) -- 1 hour
            elseif count >= 45 then
                tpz.mob.phOnDespawn(mob, ID.mob.QUU_DOMI_THE_GALLANT_PH, 4, 3000) -- 1 hour
            elseif count >= 30 then
                tpz.mob.phOnDespawn(mob, ID.mob.QUU_DOMI_THE_GALLANT_PH, 3, 3000) -- 1 hour
            else
                tpz.mob.phOnDespawn(mob, ID.mob.QUU_DOMI_THE_GALLANT_PH, 2, 3000) -- 1 hour
            end
            SetServerVariable("QUUcount", count)
        end
     end
end
