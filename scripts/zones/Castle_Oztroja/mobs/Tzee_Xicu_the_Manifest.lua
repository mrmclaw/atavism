-----------------------------------
-- Area: Castle Oztroja
--   NM: Tzee Xicu the Manifest
-- TODO: messages should be zone-wide
-- 17396137
-----------------------------------
local ID = require("scripts/zones/Castle_Oztroja/IDs")
mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/titles")
require("scripts/globals/mobs")
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
end

-- ALL ON MOB SPAWN IS NEW
function onMobSpawn(mob)
	mob:setMod(tpz.mod.MAIN_DMG_RATING, 35)
end

function onMobEngaged(mob, target)
    mob:showText(mob, ID.text.YAGUDO_KING_ENGAGE)
end

function onAdditionalEffect(mob, target, damage)
    return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.PARALYZE, {duration = 60})
end

function onMobDeath(mob, player, isKiller)
    player:addTitle(tpz.title.DEITY_DEBUNKER)
    if isKiller then
        mob:showText(mob, ID.text.YAGUDO_KING_DEATH)
    end
end

function onMobDespawn(mob)
    -- reset hqnm system back to the nm placeholder
    local nqId = mob:getID() - 3
    SetServerVariable("Tzee_Xicu_UP", 0)
    SetServerVariable("[PH]Tzee_Xicu_the_Manifest", 0)
    local wait = 72 * 3600
    SetServerVariable("[POP]Tzee_Xicu_the_Manifest", os.time() + wait) -- 3 days

    DisallowRespawn(mob:getID(), true)
    DisallowRespawn(nqId, false)
    UpdateNMSpawnPoint(nqId)
    local respawn = (75600 + ((math.random(0, 6)) * 1800)) -- 21 - 24 hours with half hour windows
    GetMobByID(nqId):setRespawnTime(respawn)
    SetServerVariable("Yagudo_Avatar_Respawn",(os.time() + respawn))
end