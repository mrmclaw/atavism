-----------------------------------
-- Area: Castle Oztroja (151)
--  Mob: Yagudo Drummer
-- Note: PH for Mee Deggi the Punisher
-----------------------------------
local ID = require("scripts/zones/Castle_Oztroja/IDs")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
end

function onMobDeath(mob)
    local MEEre = GetServerVariable("MEErespawn")
    local count = GetServerVariable("MEEcount")
    if os.time() > MEEre then
        if
            (mob:getID() == 17395798) or
            (mob:getID() == 17395766) or
            (mob:getID() == 17395783) or
            (mob:getID() == 17395775) then

            count = count + 1
            if count >= 60 then
                tpz.mob.phOnDespawn(mob, ID.mob.MEE_DEGGI_THE_PUNISHER_PH, 7, 3000) -- 1 hour
            elseif count >= 45 then
                tpz.mob.phOnDespawn(mob, ID.mob.MEE_DEGGI_THE_PUNISHER_PH, 6, 3000) -- 1 hour
            elseif count >= 30 then
                tpz.mob.phOnDespawn(mob, ID.mob.MEE_DEGGI_THE_PUNISHER_PH, 5, 3000) -- 1 hour
            else
                tpz.mob.phOnDespawn(mob, ID.mob.MEE_DEGGI_THE_PUNISHER_PH, 4, 3000) -- 1 hour
            end
            SetServerVariable("MEEcount", count)
        end
     end
end





