-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.linkshell_concierge
DROP TABLE IF EXISTS `linkshell_concierge`;
CREATE TABLE IF NOT EXISTS `linkshell_concierge` (
  `listingid` int(10) NOT NULL AUTO_INCREMENT,
  `npcid` int(10) unsigned NOT NULL DEFAULT 0,
  `linkshellid` smallint(5) unsigned NOT NULL DEFAULT 0,
  `extra` tinyblob DEFAULT NULL,
  `lslanguage` tinyint(3) unsigned NOT NULL DEFAULT 2,
  `lspearlcount` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `lsactivedays` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `lstimezone` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `lstimeofday` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `madebyplayerid` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`listingid`),
  UNIQUE KEY `listingid_UNIQUE` (`listingid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='Contains information about what linkshell the linkshell concierges have';

-- Dumping data for table atavismdb.linkshell_concierge: ~0 rows (approximately)
/*!40000 ALTER TABLE `linkshell_concierge` DISABLE KEYS */;
/*!40000 ALTER TABLE `linkshell_concierge` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
