-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.automaton_spells
DROP TABLE IF EXISTS `automaton_spells`;
CREATE TABLE IF NOT EXISTS `automaton_spells` (
  `spellid` smallint(4) unsigned NOT NULL,
  `skilllevel` smallint(3) unsigned NOT NULL DEFAULT 0,
  `heads` tinyint(2) unsigned NOT NULL DEFAULT 0,
  `enfeeble` smallint(4) unsigned NOT NULL DEFAULT 0,
  `immunity` smallint(4) unsigned NOT NULL DEFAULT 0,
  `removes` int(6) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`spellid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 AVG_ROW_LENGTH=14;

-- Dumping data for table atavismdb.automaton_spells: 50 rows
/*!40000 ALTER TABLE `automaton_spells` DISABLE KEYS */;
INSERT INTO `automaton_spells` (`spellid`, `skilllevel`, `heads`, `enfeeble`, `immunity`, `removes`) VALUES
	(1, 12, 31, 0, 0, 136129),
	(2, 45, 31, 0, 0, 0),
	(3, 81, 31, 0, 0, 0),
	(4, 147, 31, 0, 0, 0),
	(5, 207, 16, 0, 0, 0),
	(14, 27, 16, 0, 0, 3),
	(15, 36, 16, 0, 0, 4),
	(16, 45, 16, 0, 0, 5),
	(17, 60, 16, 0, 0, 6),
	(18, 120, 16, 0, 0, 7),
	(19, 105, 16, 0, 0, 2079),
	(20, 90, 16, 0, 0, 594974),
	(23, 0, 61, 134, 0, 0),
	(24, 96, 61, 134, 0, 0),
	(56, 42, 61, 13, 128, 0),
	(58, 21, 61, 4, 32, 0),
	(59, 57, 61, 6, 16, 0),
	(108, 66, 16, 0, 0, 0),
	(110, 135, 16, 0, 0, 0),
	(111, 232, 16, 0, 0, 0),
	(144, 60, 40, 0, 0, 0),
	(145, 153, 40, 0, 0, 0),
	(146, 251, 40, 0, 0, 0),
	(147, 281, 32, 0, 0, 0),
	(149, 75, 40, 0, 0, 0),
	(150, 178, 40, 0, 0, 0),
	(151, 256, 40, 0, 0, 0),
	(152, 286, 32, 0, 0, 0),
	(154, 45, 40, 0, 0, 0),
	(155, 138, 40, 0, 0, 0),
	(156, 246, 40, 0, 0, 0),
	(157, 276, 32, 0, 0, 0),
	(159, 15, 40, 0, 0, 0),
	(160, 108, 40, 0, 0, 0),
	(161, 227, 40, 0, 0, 0),
	(162, 266, 32, 0, 0, 0),
	(164, 90, 40, 0, 0, 0),
	(165, 203, 40, 0, 0, 0),
	(166, 261, 40, 0, 0, 0),
	(169, 30, 40, 0, 0, 0),
	(170, 123, 40, 0, 0, 0),
	(171, 236, 40, 0, 0, 0),
	(172, 271, 32, 0, 0, 0),
	(220, 18, 61, 3, 256, 0),
	(230, 33, 61, 135, 0, 0),
	(231, 111, 61, 135, 0, 0),
	(245, 45, 32, 0, 0, 0),
	(247, 78, 32, 0, 0, 0),
	(254, 27, 61, 5, 64, 0),
	(270, 120, 32, 140, 0, 0);
/*!40000 ALTER TABLE `automaton_spells` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
