-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.conquest_system
DROP TABLE IF EXISTS `conquest_system`;
CREATE TABLE IF NOT EXISTS `conquest_system` (
  `region_id` tinyint(2) NOT NULL DEFAULT 0,
  `region_control` tinyint(2) NOT NULL DEFAULT 0,
  `region_control_prev` tinyint(2) NOT NULL DEFAULT 0,
  `sandoria_influence` int(10) NOT NULL DEFAULT 0,
  `bastok_influence` int(10) NOT NULL DEFAULT 0,
  `windurst_influence` int(10) NOT NULL DEFAULT 0,
  `beastmen_influence` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table atavismdb.conquest_system: 19 rows
/*!40000 ALTER TABLE `conquest_system` DISABLE KEYS */;
INSERT INTO `conquest_system` (`region_id`, `region_control`, `region_control_prev`, `sandoria_influence`, `bastok_influence`, `windurst_influence`, `beastmen_influence`) VALUES
	(0, 1, 1, 1282, 3021, 53, 144),
	(1, 1, 1, 588, 1347, 1314, 1251),
	(2, 3, 1, 149, 1773, 445, 2133),
	(3, 0, 1, 1873, 1075, 1024, 528),
	(4, 1, 0, 10, 4448, 21, 21),
	(5, 2, 0, 32, 69, 4156, 243),
	(6, 2, 0, 135, 264, 2642, 1459),
	(7, 1, 0, 26, 1828, 809, 1837),
	(8, 3, 0, 172, 23, 112, 4193),
	(9, 3, 3, 1, 119, 1, 4379),
	(10, 2, 3, 112, 11, 4168, 209),
	(11, 1, 3, 15, 4382, 16, 87),
	(12, 2, 3, 9, 411, 3630, 450),
	(13, 1, 3, 22, 4430, 22, 26),
	(14, 0, 3, 2141, 1685, 376, 298),
	(15, 3, 3, 915, 982, 909, 1694),
	(16, 0, 3, 5000, 0, 0, 1000),
	(17, 3, 3, 0, 0, 0, 1000),
	(18, 3, 3, 0, 0, 0, 1000);
/*!40000 ALTER TABLE `conquest_system` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
