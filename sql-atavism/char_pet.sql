-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.char_pet
DROP TABLE IF EXISTS `char_pet`;
CREATE TABLE IF NOT EXISTS `char_pet` (
  `charid` int(10) unsigned NOT NULL,
  `wyvernid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `automatonid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `unlocked_attachments` blob DEFAULT NULL,
  `equipped_attachments` blob DEFAULT NULL,
  `adventuringfellowid` smallint(3) unsigned NOT NULL DEFAULT 0,
  `chocoboid` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.char_pet: 6 rows
/*!40000 ALTER TABLE `char_pet` DISABLE KEYS */;
INSERT INTO `char_pet` (`charid`, `wyvernid`, `automatonid`, `unlocked_attachments`, `equipped_attachments`, `adventuringfellowid`, `chocoboid`) VALUES
	(238, 31, 0, NULL, NULL, 0, 0),
	(258, 1, 0, NULL, NULL, 0, 0),
	(257, 8, 0, NULL, NULL, 0, 0),
	(275, 1, 0, NULL, NULL, 0, 0),
	(277, 14, 0, NULL, NULL, 0, 0),
	(112, 30, 0, NULL, NULL, 0, 0);
/*!40000 ALTER TABLE `char_pet` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
