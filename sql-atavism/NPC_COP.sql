-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.NPC_COP
DROP TABLE IF EXISTS `NPC_COP`;
CREATE TABLE IF NOT EXISTS `NPC_COP` (
  `polutils_name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.NPC_COP: ~36 rows (approximately)
/*!40000 ALTER TABLE `NPC_COP` DISABLE KEYS */;
INSERT INTO `NPC_COP` (`polutils_name`) VALUES
	('Bagnobrok'),
	('Blabbivix'),
	('Brita'),
	('Caruvinda'),
	('Chenon'),
	('Dabido-Sorobido'),
	('Emaliveulaux'),
	('Emila'),
	('Gallagher'),
	('Garridan'),
	('Gregory'),
	('Hayris'),
	('Hinaree'),
	('Komulili'),
	('Machu-Kuchu'),
	('Moritz'),
	('Olaky-Yayulaky '),
	('Ominous Cloud'),
	('Ophelia'),
	('Parike-Poranke'),
	('Patient Wheel'),
	('Pattsu-Yabittsu'),
	('Poudoruchant'),
	('Ravorara'),
	('Rudolfo'),
	('Sagheera'),
	('Sinon'),
	('Sohyon'),
	('Taulenne'),
	('Tesch Garanjy'),
	('Tonana'),
	('Venessa'),
	('Vishwas'),
	('Wilhelm'),
	('Wobke'),
	('Wooden Shutter');
/*!40000 ALTER TABLE `NPC_COP` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
