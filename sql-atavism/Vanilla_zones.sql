-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.Vanilla_zones
DROP TABLE IF EXISTS `Vanilla_zones`;
CREATE TABLE IF NOT EXISTS `Vanilla_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.Vanilla_zones: ~90 rows (approximately)
/*!40000 ALTER TABLE `Vanilla_zones` DISABLE KEYS */;
INSERT INTO `Vanilla_zones` (`name`) VALUES
	('unknown'),
	('West_Ronfaure'),
	('East_Ronfaure'),
	('La_Theine_Plateau'),
	('Valkurm_Dunes'),
	('Jugner_Forest'),
	('Batallia_Downs'),
	('North_Gustaberg'),
	('South_Gustaberg'),
	('Konschtat_Highlands'),
	('Pashhow_Marshlands'),
	('Rolanberry_Fields'),
	('Beaucedine_Glacier'),
	('Xarcabard'),
	('West_Sarutabaruta'),
	('East_Sarutabaruta'),
	('Tahrongi_Canyon'),
	('Buburimu_Peninsula'),
	('Meriphataud_Mountains'),
	('Sauromugue_Champaign'),
	('Qufim_Island'),
	('Behemoths_Dominion'),
	('Mordion_Gaol'),
	('Horlais_Peak'),
	('Ghelsba_Outpost'),
	('Fort_Ghelsba'),
	('Yughott_Grotto'),
	('Palborough_Mines'),
	('Waughroon_Shrine'),
	('Giddeus'),
	('Balgas_Dais'),
	('Beadeaux'),
	('Qulun_Dome'),
	('Davoi'),
	('Monastic_Cavern'),
	('Castle_Oztroja'),
	('Altar_Room'),
	('Middle_Delkfutts_Tower'),
	('Upper_Delkfutts_Tower'),
	('Castle_Zvahl_Baileys'),
	('Castle_Zvahl_Keep'),
	('Throne_Room'),
	('Ranguemont_Pass'),
	('Bostaunieux_Oubliette'),
	('Toraimarai_Canal'),
	('Zeruhn_Mines'),
	('Lower_Delkfutts_Tower'),
	('Residential_Area'),
	('King_Ranperres_Tomb'),
	('Dangruf_Wadi'),
	('Inner_Horutoto_Ruins'),
	('Ordelles_Caves'),
	('Outer_Horutoto_Ruins'),
	('The_Eldieme_Necropolis'),
	('Gusgen_Mines'),
	('Crawlers_Nest'),
	('Maze_of_Shakhrami'),
	('Residential_Area'),
	('Garlaige_Citadel'),
	('FeiYin'),
	('QuBia_Arena'),
	('GM_Home'),
	('Residential_Area'),
	('Residential_Area'),
	('Ship_bound_for_Selbina'),
	('Ship_bound_for_Mhaura'),
	('San_dOria-Jeuno_Airship'),
	('Bastok-Jeuno_Airship'),
	('Windurst-Jeuno_Airship'),
	('Southern_San_dOria'),
	('Northern_San_dOria'),
	('Port_San_dOria'),
	('Chateau_dOraguille'),
	('Bastok_Mines'),
	('Bastok_Markets'),
	('Port_Bastok'),
	('Metalworks'),
	('Windurst_Waters'),
	('Windurst_Walls'),
	('Port_Windurst'),
	('Windurst_Woods'),
	('Heavens_Tower'),
	('RuLude_Gardens'),
	('Upper_Jeuno'),
	('Lower_Jeuno'),
	('Port_Jeuno'),
	('Selbina'),
	('Mhaura'),
	('Ship_bound_for_Selbina_Pirates'),
	('Ship_bound_for_Mhaura_Pirates');
/*!40000 ALTER TABLE `Vanilla_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
