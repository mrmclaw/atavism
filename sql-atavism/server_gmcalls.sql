-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.server_gmcalls
DROP TABLE IF EXISTS `server_gmcalls`;
CREATE TABLE IF NOT EXISTS `server_gmcalls` (
  `callid` int(11) NOT NULL AUTO_INCREMENT,
  `charid` int(11) NOT NULL,
  `charname` varchar(16) NOT NULL,
  `accid` int(11) NOT NULL,
  `timesubmit` datetime NOT NULL DEFAULT current_timestamp(),
  `zoneid` int(11) NOT NULL,
  `pos_x` float NOT NULL,
  `pos_y` float NOT NULL,
  `pos_z` float NOT NULL,
  `version` varchar(128) NOT NULL,
  `message` varchar(1024) NOT NULL,
  `harassment` tinyint(4) NOT NULL,
  `stuck` tinyint(4) NOT NULL,
  `blocked` tinyint(4) NOT NULL,
  `assignee` varchar(16) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`callid`),
  KEY `charid` (`charid`),
  KEY `charname` (`charname`),
  KEY `accid` (`accid`),
  KEY `timesubmit` (`timesubmit`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.server_gmcalls: ~0 rows (approximately)
/*!40000 ALTER TABLE `server_gmcalls` DISABLE KEYS */;
/*!40000 ALTER TABLE `server_gmcalls` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
