-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.audit_chat
DROP TABLE IF EXISTS `audit_chat`;
CREATE TABLE IF NOT EXISTS `audit_chat` (
  `lineID` int(10) NOT NULL AUTO_INCREMENT,
  `speaker` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `lsName` tinytext DEFAULT NULL,
  `recipient` tinytext DEFAULT NULL,
  `message` blob DEFAULT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`lineID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.audit_chat: 0 rows
/*!40000 ALTER TABLE `audit_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_chat` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
