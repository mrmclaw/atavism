-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.COP_zones
DROP TABLE IF EXISTS `COP_zones`;
CREATE TABLE IF NOT EXISTS `COP_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.COP_zones: ~43 rows (approximately)
/*!40000 ALTER TABLE `COP_zones` DISABLE KEYS */;
INSERT INTO `COP_zones` (`name`) VALUES
	('Phanauet_Channel'),
	('Carpenters_Landing'),
	('Manaclipper'),
	('Bibiki_Bay'),
	('Uleguerand_Range'),
	('Bearclaw_Pinnacle'),
	('Attohwa_Chasm'),
	('Boneyard_Gully'),
	('PsoXja'),
	('The_Shrouded_Maw'),
	('Oldton_Movalpolos'),
	('Newton_Movalpolos'),
	('Mine_Shaft_2716'),
	('Hall_of_Transference'),
	('Promyvion-Holla'),
	('Spire_of_Holla'),
	('Promyvion-Dem'),
	('Spire_of_Dem'),
	('Promyvion-Mea'),
	('Spire_of_Mea'),
	('Promyvion-Vahzl'),
	('Spire_of_Vahzl'),
	('Lufaise_Meadows'),
	('Misareaux_Coast'),
	('Tavnazian_Safehold'),
	('Phomiuna_Aqueducts'),
	('Sacrarium'),
	('Riverne-Site_B01'),
	('Riverne-Site_A01'),
	('Monarch_Linn'),
	('Sealions_Den'),
	('AlTaieu'),
	('Grand_Palace_of_HuXzoi'),
	('The_Garden_of_RuHmet'),
	('Empyreal_Paradox'),
	('Temenos'),
	('Apollyon'),
	('Dynamis-Valkurm'),
	('Dynamis-Buburimu'),
	('Dynamis-Qufim'),
	('Dynamis-Tavnazia'),
	('Diorama_Abdhaljs-Ghelsba'),
	('Abdhaljs_Isle-Purgonorgo');
/*!40000 ALTER TABLE `COP_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
