-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.WOTG_zones
DROP TABLE IF EXISTS `WOTG_zones`;
CREATE TABLE IF NOT EXISTS `WOTG_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.WOTG_zones: ~34 rows (approximately)
/*!40000 ALTER TABLE `WOTG_zones` DISABLE KEYS */;
INSERT INTO `WOTG_zones` (`name`) VALUES
	('Southern_San_dOria_[S]'),
	('East_Ronfaure_[S]'),
	('Jugner_Forest_[S]'),
	('Vunkerl_Inlet_[S]'),
	('Batallia_Downs_[S]'),
	('La_Vaule_[S]'),
	('Everbloom_Hollow'),
	('Bastok_Markets_[S]'),
	('North_Gustaberg_[S]'),
	('Grauberg_[S]'),
	('Pashhow_Marshlands_[S]'),
	('Rolanberry_Fields_[S]'),
	('Beadeaux_[S]'),
	('Ruhotz_Silvermines'),
	('Windurst_Waters_[S]'),
	('West_Sarutabaruta_[S]'),
	('Fort_Karugo-Narugo_[S]'),
	('Meriphataud_Mountains_[S]'),
	('Sauromugue_Champaign_[S]'),
	('Castle_Oztroja_[S]'),
	('Ghoyus_Reverie'),
	('Beaucedine_Glacier_[S]'),
	('Xarcabard_[S]'),
	('Castle_Zvahl_Baileys_[S]'),
	('Castle_Zvahl_Keep_[S]'),
	('Throne_Room_[S]'),
	('Garlaige_Citadel_[S]'),
	('Crawlers_Nest_[S]'),
	('The_Eldieme_Necropolis_[S]'),
	('Walk_of_Echoes'),
	('Provenance'),
	('Walk_of_Echoes_[P2]'),
	('Walk_of_Echoes_[P1]'),
	('The_Shrouded_Maw');
/*!40000 ALTER TABLE `WOTG_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
