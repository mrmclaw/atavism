-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.SOA_zones
DROP TABLE IF EXISTS `SOA_zones`;
CREATE TABLE IF NOT EXISTS `SOA_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.SOA_zones: ~26 rows (approximately)
/*!40000 ALTER TABLE `SOA_zones` DISABLE KEYS */;
INSERT INTO `SOA_zones` (`name`) VALUES
	('Western_Adoulin'),
	('Eastern_Adoulin'),
	('Rala_Waterways'),
	('Rala_Waterways_U'),
	('Yahse_Hunting_Grounds'),
	('Ceizak_Battlegrounds'),
	('Foret_de_Hennetiel'),
	('Yorcia_Weald'),
	('Yorcia_Weald_U'),
	('Morimar_Basalt_Fields'),
	('Marjami_Ravine'),
	('Kamihr_Drifts'),
	('Sih_Gates'),
	('Moh_Gates'),
	('Cirdas_Caverns'),
	('Cirdas_Caverns_U'),
	('Dho_Gates'),
	('Woh_Gates'),
	('Outer_RaKaznar'),
	('Outer_RaKaznar_U'),
	('RaKaznar_Inner_Court'),
	('RaKaznar_Turris'),
	('Mog_Garden'),
	('Leafallia'),
	('Mount_Kamihr'),
	('Celennia_Memorial_Library');
/*!40000 ALTER TABLE `SOA_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
