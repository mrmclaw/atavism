-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.ROV_zones
DROP TABLE IF EXISTS `ROV_zones`;
CREATE TABLE IF NOT EXISTS `ROV_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.ROV_zones: ~10 rows (approximately)
/*!40000 ALTER TABLE `ROV_zones` DISABLE KEYS */;
INSERT INTO `ROV_zones` (`name`) VALUES
	('Escha_ZiTah'),
	('Escha_RuAun'),
	('Desuetia_Empyreal_Paradox'),
	('Reisenjima'),
	('Reisenjima_Henge'),
	('Reisenjima_Sanctorium'),
	('Dynamis-San_dOria_[D]'),
	('Dynamis-Bastok_[D]'),
	('Dynamis-Windurst_[D]'),
	('Dynamis-Jeuno_[D]');
/*!40000 ALTER TABLE `ROV_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
