-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.NPC_TOAU
DROP TABLE IF EXISTS `NPC_TOAU`;
CREATE TABLE IF NOT EXISTS `NPC_TOAU` (
  `polutils_name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.NPC_TOAU: ~16 rows (approximately)
/*!40000 ALTER TABLE `NPC_TOAU` DISABLE KEYS */;
INSERT INTO `NPC_TOAU` (`polutils_name`) VALUES
	('Ajithaam'),
	('Alib-Mufalib'),
	('Amutiyaal'),
	('Faursel'),
	('Finbarr'),
	('Gwinar'),
	('Hantileon'),
	('Ibwam'),
	('Nuria'),
	('Pulonono'),
	('Rumoie'),
	('Shamarhaan'),
	('Valkeng'),
	('Zalsuhm'),
	('Zelala'),
	('Zopago');
/*!40000 ALTER TABLE `NPC_TOAU` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
