-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.Abyssea_zones
DROP TABLE IF EXISTS `Abyssea_zones`;
CREATE TABLE IF NOT EXISTS `Abyssea_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.Abyssea_zones: ~10 rows (approximately)
/*!40000 ALTER TABLE `Abyssea_zones` DISABLE KEYS */;
INSERT INTO `Abyssea_zones` (`name`) VALUES
	('Abyssea-Konschtat'),
	('Abyssea-Tahrongi'),
	('Abyssea-La_Theine'),
	('Abyssea-Attohwa'),
	('Abyssea-Misareaux'),
	('Abyssea-Vunkerl'),
	('Abyssea-Altepa'),
	('Abyssea-Uleguerand'),
	('Abyssea-Grauberg'),
	('Abyssea-Empyreal_Paradox');
/*!40000 ALTER TABLE `Abyssea_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
