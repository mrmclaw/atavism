-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.flist
DROP TABLE IF EXISTS `flist`;
CREATE TABLE IF NOT EXISTS `flist` (
  `callingchar` int(10) unsigned NOT NULL DEFAULT 0,
  `listedchar` int(10) unsigned NOT NULL DEFAULT 0,
  `status` int(4) NOT NULL DEFAULT 0,
  `name` varchar(32) DEFAULT NULL,
  `note` varchar(16) DEFAULT 'actualempty'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.flist: ~10 rows (approximately)
/*!40000 ALTER TABLE `flist` DISABLE KEYS */;
INSERT INTO `flist` (`callingchar`, `listedchar`, `status`, `name`, `note`) VALUES
	(10, 20, 0, 'Buttercup', 'actualempty'),
	(10, 3, 0, 'Wobbles', 'actualempty'),
	(20, 10, 0, 'Lihtah', 'actualempty'),
	(10, 33, 0, 'Luna', 'actualempty'),
	(10, 18, 0, 'Glenevere', 'actualempty'),
	(18, 10, 0, 'Lihtah', 'actualempty'),
	(18, 20, 0, 'Buttercup', 'actualempty'),
	(10, 5, 0, 'Dionysus', 'actualempty'),
	(10, 38, 0, 'Alys', 'actualempty'),
	(10, 46, 0, 'Reil', 'actualempty');
/*!40000 ALTER TABLE `flist` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
