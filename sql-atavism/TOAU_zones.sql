-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.TOAU_zones
DROP TABLE IF EXISTS `TOAU_zones`;
CREATE TABLE IF NOT EXISTS `TOAU_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.TOAU_zones: ~33 rows (approximately)
/*!40000 ALTER TABLE `TOAU_zones` DISABLE KEYS */;
INSERT INTO `TOAU_zones` (`name`) VALUES
	('Open_sea_route_to_Al_Zahbi'),
	('Open_sea_route_to_Mhaura'),
	('Al_Zahbi'),
	('Aht_Urhgan_Whitegate'),
	('Wajaom_Woodlands'),
	('Bhaflau_Thickets'),
	('Nashmau'),
	('Arrapago_Reef'),
	('Ilrusi_Atoll'),
	('Periqia'),
	('Talacca_Cove'),
	('Silver_Sea_route_to_Nashmau'),
	('Silver_Sea_route_to_Al_Zahbi'),
	('The_Ashu_Talif'),
	('Mount_Zhayolm'),
	('Halvung'),
	('Lebros_Cavern'),
	('Navukgo_Execution_Chamber'),
	('Mamook'),
	('Mamool_Ja_Training_Grounds'),
	('Jade_Sepulcher'),
	('Aydeewa_Subterrane'),
	('Leujaoam_Sanctum'),
	('Chocobo_Circuit'),
	('The_Colosseum'),
	('Alzadaal_Undersea_Ruins'),
	('Zhayolm_Remnants'),
	('Arrapago_Remnants'),
	('Bhaflau_Remnants'),
	('Silver_Sea_Remnants'),
	('Nyzul_Isle'),
	('Hazhalm_Testing_Grounds'),
	('Caedarva_Mire');
/*!40000 ALTER TABLE `TOAU_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
