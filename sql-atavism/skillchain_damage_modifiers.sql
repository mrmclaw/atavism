-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.skillchain_damage_modifiers
DROP TABLE IF EXISTS `skillchain_damage_modifiers`;
CREATE TABLE IF NOT EXISTS `skillchain_damage_modifiers` (
  `chain_level` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `chain_count` enum('1','2','3','4','5') NOT NULL DEFAULT '1',
  `initial_modifier` int(3) NOT NULL DEFAULT 1,
  `magic_burst_modifier` int(3) NOT NULL DEFAULT 1,
  PRIMARY KEY (`chain_level`,`chain_count`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.skillchain_damage_modifiers: 20 rows
/*!40000 ALTER TABLE `skillchain_damage_modifiers` DISABLE KEYS */;
INSERT INTO `skillchain_damage_modifiers` (`chain_level`, `chain_count`, `initial_modifier`, `magic_burst_modifier`) VALUES
	('1', '1', 500, 1300),
	('1', '2', 600, 1350),
	('1', '3', 700, 1400),
	('1', '4', 800, 1450),
	('1', '5', 900, 1500),
	('2', '1', 600, 1300),
	('2', '2', 750, 1350),
	('2', '3', 1000, 1400),
	('2', '4', 1250, 1450),
	('2', '5', 1500, 1500),
	('3', '1', 1000, 1300),
	('3', '2', 1500, 1350),
	('3', '3', 1750, 1400),
	('3', '4', 2000, 1450),
	('3', '5', 2250, 1500),
	('4', '1', 1000, 1300),
	('4', '2', 1500, 1350),
	('4', '3', 1750, 1400),
	('4', '4', 2000, 1450),
	('4', '5', 2250, 1500);
/*!40000 ALTER TABLE `skillchain_damage_modifiers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
