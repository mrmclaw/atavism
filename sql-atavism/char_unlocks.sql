-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.char_unlocks
DROP TABLE IF EXISTS `char_unlocks`;
CREATE TABLE IF NOT EXISTS `char_unlocks` (
  `charid` int(10) unsigned NOT NULL,
  `outpost_sandy` int(10) unsigned NOT NULL DEFAULT 0,
  `outpost_bastok` int(10) unsigned NOT NULL DEFAULT 0,
  `outpost_windy` int(10) unsigned NOT NULL DEFAULT 0,
  `mog_locker` int(10) unsigned NOT NULL DEFAULT 0,
  `runic_portal` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `maw` int(10) unsigned NOT NULL DEFAULT 0,
  `campaign_sandy` int(10) unsigned NOT NULL DEFAULT 0,
  `campaign_bastok` int(10) unsigned NOT NULL DEFAULT 0,
  `campaign_windy` int(10) unsigned NOT NULL DEFAULT 0,
  `homepoints` blob DEFAULT NULL,
  `survivals` blob DEFAULT NULL,
  PRIMARY KEY (`charid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table atavismdb.char_unlocks: 504 rows
/*!40000 ALTER TABLE `char_unlocks` DISABLE KEYS */;
INSERT INTO `char_unlocks` (`charid`, `outpost_sandy`, `outpost_bastok`, `outpost_windy`, `mog_locker`, `runic_portal`, `maw`, `campaign_sandy`, `campaign_bastok`, `campaign_windy`, `homepoints`, `survivals`) VALUES
	(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(3, 0, 0, 3072, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(4, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(5, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(6, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(7, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(8, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(9, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(10, 0, 3936, 7488, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(11, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(12, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(13, 0, 0, 3072, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(14, 0, 0, 7168, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(15, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(16, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(17, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(18, 160, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(19, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(20, 0, 65504, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(21, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(22, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(23, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(24, 0, 256, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(25, 160, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(26, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(27, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(28, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(29, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(30, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(31, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(32, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(33, 39520, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(34, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(35, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(36, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(37, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(38, 0, 57312, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(39, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(40, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(41, 0, 0, 3072, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(42, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(43, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(44, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(45, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(46, 0, 352, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(47, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(48, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(49, 0, 49120, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(50, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(51, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(52, 0, 8160, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(53, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(54, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(55, 0, 65504, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(56, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(57, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(58, 0, 8032, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(59, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(60, 0, 0, 7200, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(61, 0, 0, 7200, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(62, 0, 7264, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(63, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(64, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(65, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(66, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(67, 0, 65248, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(68, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(69, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(70, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(71, 0, 65504, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(72, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(73, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(74, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(75, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(76, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(77, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(78, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(79, 0, 46016, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(80, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(81, 0, 45760, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(82, 0, 46016, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(83, 0, 33344, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(84, 0, 65504, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(85, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(86, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(87, 36448, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(88, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(89, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(90, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(91, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(92, 0, 40672, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(93, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(94, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(95, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(96, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(97, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(98, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(99, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(100, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(101, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(102, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(103, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(104, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(105, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(106, 0, 64448, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(107, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(108, 0, 33472, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(109, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(110, 0, 46016, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(111, 0, 46016, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(112, 0, 630752, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(113, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(114, 0, 320, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(115, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(116, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(117, 0, 0, 40544, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(118, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(119, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(120, 0, 0, 2048, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(121, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(122, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(123, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(124, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(125, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(126, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(127, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(128, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(129, 0, 2496, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(130, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(131, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(132, 0, 64, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(133, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(134, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(135, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(136, 0, 0, 40800, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(137, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(138, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(139, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(140, 0, 64, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(141, 0, 37568, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(142, 0, 37568, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(143, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(144, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(145, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(146, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(147, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(148, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(149, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(150, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(151, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(152, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(153, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(154, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(155, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(156, 0, 37568, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(157, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(158, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(159, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(160, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(161, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(162, 0, 37568, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(163, 0, 37568, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(164, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(165, 40544, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(166, 0, 37824, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(167, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(168, 0, 37824, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(169, 0, 37824, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(170, 0, 37824, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(171, 0, 37824, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(172, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(173, 0, 37824, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(174, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(175, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(176, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(177, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(178, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(179, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(180, 0, 0, 40800, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(181, 0, 0, 1344, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(182, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(183, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(184, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(185, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(186, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(187, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(188, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(189, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(190, 0, 256, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(191, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(192, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(193, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(194, 1056, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(195, 0, 64, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(196, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(197, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(198, 0, 37568, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(199, 0, 37824, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(200, 0, 256, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(201, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(202, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(203, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(204, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(205, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(206, 0, 0, 1088, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(207, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(208, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(209, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(210, 1056, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(211, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(212, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(213, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(214, 0, 320, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(215, 0, 64, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(216, 0, 64, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(217, 0, 320, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(218, 0, 106464, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(219, 0, 40416, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(220, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(221, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(222, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(223, 0, 320, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(224, 0, 320, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(225, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(226, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(227, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(228, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(229, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(230, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(231, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(232, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(233, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(234, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(235, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(236, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(237, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(238, 0, 106464, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(239, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(240, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(241, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(242, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(243, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(244, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(245, 0, 57312, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(246, 0, 57056, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(247, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(248, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(249, 0, 0, 1024, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(250, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(251, 0, 64, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(252, 0, 320, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(253, 0, 630752, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(254, 0, 630752, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(255, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(256, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(257, 0, 630752, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(258, 0, 630752, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(259, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(260, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(261, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(262, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(263, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(264, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(265, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(266, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(267, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(268, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(269, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(270, 0, 0, 1024, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(271, 0, 0, 1024, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(272, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(273, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(274, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(275, 0, 630752, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(276, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(277, 0, 630752, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(278, 0, 0, 40800, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(279, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(280, 0, 0, 40800, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(281, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(282, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(283, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(284, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(285, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(286, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(287, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(288, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(289, 0, 2400, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(290, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(291, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(292, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(293, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(294, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(295, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(296, 512, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(297, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(298, 512, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(299, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(300, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(301, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(302, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(303, 0, 256, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(304, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(305, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(306, 0, 2144, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(307, 0, 2144, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(308, 0, 627264, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(309, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(310, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(311, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(312, 6240, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(313, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(314, 0, 0, 696288, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(315, 0, 0, 696288, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(316, 0, 0, 565216, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(317, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(318, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(319, 39520, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(320, 39520, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(321, 0, 39360, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(322, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(323, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(324, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(325, 0, 0, 1024, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(326, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(327, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(328, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(329, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(330, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(331, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(332, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(333, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(334, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(335, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(336, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(337, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(338, 2048, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(339, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(340, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(341, 39520, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(342, 0, 0, 8032, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(343, 0, 40416, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(344, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(345, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(346, 0, 0, 696288, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(347, 0, 0, 1088, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(348, 0, 64, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(349, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(350, 0, 256, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(351, 0, 256, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(352, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(353, 0, 768, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(354, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(355, 0, 40928, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(356, 0, 0, 40928, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(357, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(358, 39008, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(359, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(360, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(361, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(362, 0, 0, 3904, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(363, 0, 0, 696288, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(364, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(365, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(366, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(367, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(368, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(369, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(370, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(371, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(372, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(373, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(374, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(375, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(376, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(377, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(378, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(379, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(380, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(381, 0, 40160, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(382, 0, 40160, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(383, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(384, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(385, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(386, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(387, 0, 0, 256, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(388, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(389, 0, 768, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(390, 32864, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(391, 0, 768, 7264, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(392, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(393, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(394, 0, 0, 3168, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(395, 0, 0, 64, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(396, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(397, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(398, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(399, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(400, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(401, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(402, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(403, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(404, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(405, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(406, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(407, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(408, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(409, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(410, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(411, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(412, 0, 0, 7168, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(413, 0, 0, 1024, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(414, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(415, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(416, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(417, 0, 256, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(418, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(419, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(420, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(421, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(422, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(423, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(424, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(425, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(426, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(427, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(428, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(429, 0, 35296, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(430, 32, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(431, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(432, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(433, 0, 40416, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(434, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(435, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(436, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(437, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(438, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(439, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(440, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(441, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(442, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(443, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(444, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(445, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(446, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(447, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(448, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(449, 4640, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(450, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(451, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(452, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(453, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(454, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(455, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(456, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(457, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(458, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(459, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(460, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(461, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(462, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(463, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(464, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(465, 0, 0, 3136, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(466, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(467, 0, 0, 3136, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(468, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(469, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(470, 0, 0, 696256, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(471, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(472, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(473, 0, 627520, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(474, 0, 627520, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(475, 0, 627520, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(476, 0, 627520, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(477, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(478, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(479, 0, 66368, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(480, 0, 66368, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(481, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(482, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(483, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(484, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(485, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(486, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(487, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(488, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(489, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(490, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(491, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(492, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(493, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(494, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(495, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(496, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(497, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(498, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(499, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(500, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(501, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(502, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(503, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
	(504, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL);
/*!40000 ALTER TABLE `char_unlocks` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
