-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.cheat_incidents
DROP TABLE IF EXISTS `cheat_incidents`;
CREATE TABLE IF NOT EXISTS `cheat_incidents` (
  `charid` int(10) unsigned NOT NULL,
  `incident_time` datetime NOT NULL DEFAULT current_timestamp(),
  `cheatid` int(10) unsigned NOT NULL,
  `cheatarg` int(10) unsigned NOT NULL,
  `description` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.cheat_incidents: 34 rows
/*!40000 ALTER TABLE `cheat_incidents` DISABLE KEYS */;
INSERT INTO `cheat_incidents` (`charid`, `incident_time`, `cheatid`, `cheatarg`, `description`) VALUES
	(165, '2021-12-20 23:19:04', 2, 43818, 'Pusa went over the speed limit: 438.181244 (raw=1314.543701, time=3, threshold=60.000000)'),
	(242, '2021-12-22 21:41:14', 2, 14861, 'Lds went over the speed limit: 148.611115 (raw=297.222229, time=2, threshold=60.000000)'),
	(295, '2022-01-15 01:29:36', 2, 22622, 'Durex went over the speed limit: 226.222168 (raw=904.888672, time=4, threshold=60.000000)'),
	(181, '2022-01-24 20:25:33', 2, 29397, 'Poe went over the speed limit: 293.971741 (raw=1469.858765, time=5, threshold=60.000000)'),
	(277, '2022-01-26 04:17:44', 1, 7494, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(254, '2022-02-02 09:53:50', 1, 2825, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(315, '2022-02-07 00:47:12', 1, 2460, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(389, '2022-02-08 19:22:20', 3, 100, 'Fishbot detection token mismatch.'),
	(180, '2022-02-08 19:22:25', 3, 241, 'Fishbot detection token mismatch.'),
	(390, '2022-02-08 19:22:39', 3, 100, 'Fishbot detection token mismatch.'),
	(312, '2022-02-08 22:35:56', 3, 240, 'Fishbot detection token mismatch.'),
	(254, '2022-02-15 02:47:15', 1, 2894, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(165, '2022-02-15 09:32:02', 2, 98654, 'Pusa went over the speed limit: 986.540710 (raw=986.540710, time=1, threshold=120.000000)'),
	(316, '2022-03-01 01:36:18', 1, 2830, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(316, '2022-03-17 20:15:29', 1, 8492, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-03-26 07:42:44', 1, 3080, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-03-26 07:43:21', 1, 3485, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-03-26 07:44:02', 1, 3468, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(182, '2022-03-27 08:36:08', 2, 41898, 'Lucavie went over the speed limit: 418.981171 (raw=1675.924683, time=4, threshold=60.000000)'),
	(316, '2022-04-03 21:45:31', 1, 1640, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(346, '2022-04-10 09:51:47', 1, 3708, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-12 03:48:05', 1, 4210, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(315, '2022-04-14 20:39:48', 1, 7806, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:50:57', 1, 3961, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:51:38', 1, 3187, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:52:50', 1, 3307, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:54:55', 1, 3729, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:56:38', 1, 4471, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:57:28', 1, 2996, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:58:18', 1, 3601, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:59:02', 1, 3325, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 05:59:45', 1, 3151, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(112, '2022-04-16 06:00:31', 1, 6404, 'Player attempted to bypass synth animation by injecting synth done packet.'),
	(314, '2022-04-24 00:36:21', 1, 2829, 'Player attempted to bypass synth animation by injecting synth done packet.');
/*!40000 ALTER TABLE `cheat_incidents` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
