-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.ROZ_zones
DROP TABLE IF EXISTS `ROZ_zones`;
CREATE TABLE IF NOT EXISTS `ROZ_zones` (
  `name` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atavismdb.ROZ_zones: ~45 rows (approximately)
/*!40000 ALTER TABLE `ROZ_zones` DISABLE KEYS */;
INSERT INTO `ROZ_zones` (`name`) VALUES
	('Cape_Teriggan'),
	('Eastern_Altepa_Desert'),
	('The_Sanctuary_of_ZiTah'),
	('RoMaeve'),
	('Yuhtunga_Jungle'),
	('Yhoator_Jungle'),
	('Western_Altepa_Desert'),
	('Valley_of_Sorrows'),
	('RuAun_Gardens'),
	('Dynamis-Beaucedine'),
	('Dynamis-Xarcabard'),
	('The_Boyahda_Tree'),
	('Dragons_Aery'),
	('Temple_of_Uggalepih'),
	('Den_of_Rancor'),
	('Sacrificial_Chamber'),
	('Chamber_of_Oracles'),
	('Full_Moon_Fountain'),
	('Korroloka_Tunnel'),
	('Kuftal_Tunnel'),
	('Sea_Serpent_Grotto'),
	('VeLugannon_Palace'),
	('The_Shrine_of_RuAvitau'),
	('Stellar_Fulcrum'),
	('LaLoff_Amphitheater'),
	('The_Celestial_Nexus'),
	('Dynamis-San_dOria'),
	('Dynamis-Bastok'),
	('Dynamis-Windurst'),
	('Dynamis-Jeuno'),
	('Cloister_of_Gales'),
	('Cloister_of_Storms'),
	('Cloister_of_Frost'),
	('Ifrits_Cauldron'),
	('Cloister_of_Flames'),
	('Quicksand_Caves'),
	('Cloister_of_Tremors'),
	('Cloister_of_Tides'),
	('Gustav_Tunnel'),
	('Labyrinth_of_Onzozo'),
	('Kazham-Jeuno_Airship'),
	('Rabao'),
	('Kazham'),
	('Hall_of_the_Gods'),
	('Norg');
/*!40000 ALTER TABLE `ROZ_zones` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
