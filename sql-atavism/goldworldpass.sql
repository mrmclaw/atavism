-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.goldworldpass
DROP TABLE IF EXISTS `goldworldpass`;
CREATE TABLE IF NOT EXISTS `goldworldpass` (
  `passid` varchar(16) NOT NULL,
  `creator_contentid` int(10) unsigned NOT NULL,
  `creator_charid` int(10) unsigned NOT NULL,
  `creation_time` datetime NOT NULL DEFAULT current_timestamp(),
  `expiry_time` timestamp NULL DEFAULT NULL,
  `user_contentid` int(10) unsigned DEFAULT NULL,
  `user_charid` int(10) unsigned DEFAULT NULL,
  `usage_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`passid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.goldworldpass: ~0 rows (approximately)
/*!40000 ALTER TABLE `goldworldpass` DISABLE KEYS */;
/*!40000 ALTER TABLE `goldworldpass` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
