-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.water_points
DROP TABLE IF EXISTS `water_points`;
CREATE TABLE IF NOT EXISTS `water_points` (
  `waterid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zoneid` smallint(3) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pointid` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `pos_x` float(7,2) NOT NULL DEFAULT 0.00,
  `pos_y` float(7,2) NOT NULL DEFAULT 0.00,
  `pos_z` float(7,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`waterid`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.water_points: 1 rows
/*!40000 ALTER TABLE `water_points` DISABLE KEYS */;
INSERT INTO `water_points` (`waterid`, `zoneid`, `type`, `pointid`, `pos_x`, `pos_y`, `pos_z`) VALUES
	(1, 100, 0, 0, 0.00, 0.00, 0.00);
/*!40000 ALTER TABLE `water_points` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
