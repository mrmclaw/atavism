-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.cheat_types
DROP TABLE IF EXISTS `cheat_types`;
CREATE TABLE IF NOT EXISTS `cheat_types` (
  `cheatid` int(10) unsigned NOT NULL,
  `name` varchar(15) NOT NULL,
  `description` varchar(128) NOT NULL,
  `argument` int(10) unsigned NOT NULL,
  `action_bitmask` int(10) unsigned NOT NULL,
  `warning_message` varchar(128) NOT NULL,
  PRIMARY KEY (`cheatid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.cheat_types: 6 rows
/*!40000 ALTER TABLE `cheat_types` DISABLE KEYS */;
INSERT INTO `cheat_types` (`cheatid`, `name`, `description`, `argument`, `action_bitmask`, `warning_message`) VALUES
	(1, 'FASTSYNTH', 'Player injects a synth done (0x59) packet to perform synth and craft skillups faster than the game allows.', 0, 7, 'A fast craft cheating attempt has been attempted. Successful craft has been blocked and the incident will be reported.'),
	(2, 'POSHACK', 'The player used an addon to teleport into a different position in the zone.', 0, 9, 'You have been caught speed / position hacking. Your account will be banned.'),
	(3, 'FISHBOT', 'Player is using an automated fishing tool', 0, 3, 'Fishing bot detected. You have been banned from fishing.'),
	(4, 'DIGBOT', 'Player is using an automated digging tool', 0, 9, 'Chobobo digging bot detected. This incident will be reported.'),
	(5, 'CLAIMBOT', 'Player is using an automated mob claiming tool', 0, 9, 'Claim bot tool detected. This incident will be reported.'),
	(6, 'DIGSKIP', 'Player is using a tool to speed up/skip the dig animation', 0, 13, 'A third-party tool was used to speed up or skip the digging animation. Further infractions will result in autojail.');
/*!40000 ALTER TABLE `cheat_types` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
