-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.7.3-MariaDB - Arch Linux
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table atavismdb.char_gmmessage
DROP TABLE IF EXISTS `char_gmmessage`;
CREATE TABLE IF NOT EXISTS `char_gmmessage` (
  `messageid` int(11) NOT NULL AUTO_INCREMENT,
  `callid` int(11) NOT NULL,
  `charid` int(11) NOT NULL,
  `accid` int(11) NOT NULL,
  `gmid` int(11) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT current_timestamp(),
  `message` varchar(1024) NOT NULL,
  `read` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`messageid`),
  KEY `callid` (`callid`),
  KEY `charid` (`charid`),
  KEY `datetime` (`datetime`),
  KEY `read` (`read`),
  KEY `accid` (`accid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table atavismdb.char_gmmessage: ~0 rows (approximately)
/*!40000 ALTER TABLE `char_gmmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `char_gmmessage` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
