-- --------------------------------
-- Dynamis-Windurst SQL Drop Tables --
-- --------------------------------

-- Dynamis Windurst

-- Normal Beastmen Mobs - Vanguards (2553)
DELETE FROM mob_droplist WHERE dropid = "2553" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15072, 77); -- Adds WAR Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15134, 77); -- Adds WHM Feet
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15105, 77); -- Adds BLM Hands
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15077, 77); -- Adds THF Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15138, 77); -- Adds PLD Feet
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15109, 77); -- Adds DRK Hands
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15080, 77); -- Adds BST Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15112, 77); -- Adds RNG Hands
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15128, 77); -- Adds Sam Legs
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15084, 77); -- Adds Nin Head
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15131, 77); -- Adds SMN Legs
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 11382, 77); -- Adds BLU Feet
INSERT INTO mob_droplist VALUES(2553, 1, 1, 50, 15031, 76); -- Adds PUP Hands
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1518, 10); -- Adds colossal skull
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1466, 10); -- Adds relic iron
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1464, 10); -- Adds lancewood log
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1449, 240); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1449, 150); -- Adds 2 shell 
INSERT INTO mob_droplist VALUES(2553, 0, 0, 1000, 1449, 100); -- Adds 3 shell
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18266, 250); -- Adds relic dagger
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18260, 250); -- Adds relic knuckles
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18320, 250); -- Adds relic maul
INSERT INTO mob_droplist VALUES(2553, 1, 2, 50, 18272, 250); -- Adds relic sword

-- NM Beastmen Mobs - 1560
DELETE FROM mob_droplist WHERE dropid = "1560" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15072, 77); -- Adds WAR Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15134, 77); -- Adds WHM Feet
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15105, 77); -- Adds BLM Hands
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15077, 77); -- Adds THF Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15138, 77); -- Adds PLD Feet
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15109, 77); -- Adds DRK Hands
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15080, 77); -- Adds BST Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15112, 77); -- Adds RNG Hands
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15128, 77); -- Adds Sam Legs
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15084, 77); -- Adds Nin Head
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15131, 77); -- Adds SMN Legs
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 11382, 77); -- Adds BLU Feet
INSERT INTO mob_droplist VALUES(1560, 1, 1, 50, 15031, 76); -- Adds PUP Hands
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1518, 10); -- Adds colossal skull
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1466, 10); -- Adds relic iron
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1464, 10); -- Adds lancewood log
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1449, 240); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1449, 150); -- Adds 2 shell 
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1449, 100); -- Adds 3 shell
INSERT INTO mob_droplist VALUES(1560, 0, 0, 1000, 1450, 50); -- Adds 1 Lungo 

-- Icons - 195
DELETE FROM mob_droplist WHERE dropid = "195"; -- Delete
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(195, 0, 0, 1000, 1450, 50); -- Adds 1 Lungo 

-- Megaboss - 2510
DELETE FROM mob_droplist WHERE dropid = "2510"; -- Delete
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1449, 150); -- Adds 1 shell 
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1449, 150); -- Adds 2 shell 
INSERT INTO mob_droplist VALUES(2510, 0, 0, 1000, 1450, 150); -- Adds 1 Lungo 

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Arch_Tzee_Xicu_Idol";
UPDATE mob_groups SET dropid = "195" WHERE zoneid = "187" AND name = "Avatar_Icon";
UPDATE mob_groups SET dropid = "195" WHERE zoneid = "187" AND name = "Avatar_Idol";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Fuu_Tzapo_the_Blessed";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Haa_Pevi_the_Stentorian";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Loo_Hepe_the_Eyepiercer";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Maa_Febi_the_Steadfast";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Muu_Febi_the_Steadfast";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Naa_Yixo_the_Stillrage";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Tee_Zaksa_the_Ceaseless";
UPDATE mob_groups SET dropid = "2510" WHERE zoneid = "187" AND name = "Tzee_Xicu_Idol";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Vanguards_Avatar";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Vanguards_Crow";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Vanguards_Wyvern";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Assassin";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Chanter";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Exemplar";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Inciter";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Liberator";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Ogresoother";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Oracle";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Partisan";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Persecutor";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Prelate";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Priest";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Salvager";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Sentinel";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Skirmisher";
UPDATE mob_groups SET dropid = "2553" WHERE zoneid = "187" AND name = "Vanguard_Visionary";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Wuu_Qoho_the_Razorclaw";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Xoo_Kaza_the_Solemn";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "187" AND name = "Xuu_Bhoqas_Avatar";
UPDATE mob_groups SET dropid = "1560" WHERE zoneid = "187" AND name = "Xuu_Bhoqa_the_Enigma";
