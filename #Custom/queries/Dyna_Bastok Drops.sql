-- --------------------------------
-- Dynamis-Bastok SQL Drop Tables --
-- --------------------------------

-- Dynamis Bastok

-- Normal Beastmen Mobs - Vanguards (2558)
DELETE FROM mob_droplist WHERE dropid = "2558" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15133, 77); -- Adds MNK Feet
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15120, 77); -- Adds BLM Legs
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15106, 77); -- Adds RDM Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15092, 77); -- Adds THF Body
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15078, 77); -- Adds PLD Head
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15139, 77); -- Adds DRK Feet
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15095, 77); -- Adds BST Body
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15111, 77); -- Adds BRD Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15113, 77); -- Adds SAM Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15130, 77); -- Adds DRG Legs
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 15116, 77); -- Adds SMN Hands
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 16346, 77); -- Adds BLU Legs
INSERT INTO mob_droplist VALUES(2558, 1, 1, 50, 11385, 76); -- Adds COR Feet
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1469, 10); -- Adds chunk_of_wootz_ore
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1521, 10); -- Adds vial_of_slime_juice
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1455, 240); -- Adds 1 bynes 
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1455, 150); -- Adds 2 bynes 
INSERT INTO mob_droplist VALUES(2558, 0, 0, 1000, 1455, 100); -- Adds 3 bynes
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18314, 250); -- Adds Ito
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18302, 250); -- Adds relic scythe
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18284, 250); -- Adds relic axe
INSERT INTO mob_droplist VALUES(2558, 1, 2, 50, 18278, 250); -- Adds relic blade

-- NM Beastmen Mobs - 2907
DELETE FROM mob_droplist WHERE dropid = "2907" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15133, 77); -- Adds MNK Feet
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15120, 77); -- Adds BLM Legs
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15106, 77); -- Adds RDM Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15092, 77); -- Adds THF Body
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15078, 77); -- Adds PLD Head
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15139, 77); -- Adds DRK Feet
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15095, 77); -- Adds BST Body
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15111, 77); -- Adds BRD Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15113, 77); -- Adds SAM Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15130, 77); -- Adds DRG Legs
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 15116, 77); -- Adds SMN Hands
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 16346, 77); -- Adds BLU Legs
INSERT INTO mob_droplist VALUES(2907, 1, 1, 50, 11385, 76); -- Adds COR Feet
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1469, 10); -- Adds chunk_of_wootz_ore
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1521, 10); -- Adds vial_of_slime_juice
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1455, 240); -- Adds 1 bynes 
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1455, 150); -- Adds 2 bynes 
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1455, 100); -- Adds 3 bynes
INSERT INTO mob_droplist VALUES(2907, 0, 0, 1000, 1456, 50); -- Adds 1 Hbyne

-- Effigy - 20
DELETE FROM mob_droplist WHERE dropid = "20"; -- Delete
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(20, 0, 0, 1000, 1456, 50); -- Adds 1 Hbyne

-- Megaboss - 2906
DELETE FROM mob_droplist WHERE dropid = "2906"; -- Delete
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1455, 150); -- Adds 1 bynes 
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1455, 150); -- Adds 2 bynes 
INSERT INTO mob_droplist VALUES(2906, 0, 0, 1000, 1456, 150); -- Adds 1 Hbyne

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "20" WHERE zoneid = "186" AND name = "Adamantking_Effigy";
UPDATE mob_groups SET dropid = "20" WHERE zoneid = "186" AND name = "Adamantking_Image";
UPDATE mob_groups SET dropid = "2906" WHERE zoneid = "186" AND name = "Arch_GuDha_Effigy";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "BuBho_Truesteel";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "GiPha_Manameister";
UPDATE mob_groups SET dropid = "2906" WHERE zoneid = "186" AND name = "GuDha_Effigy";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "GuNhi_Noondozer";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "KoDho_Cannonball";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "RaGhos_Avatar";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "RaGho_Darkfount";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "Vanguards_Avatar";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "Vanguards_Scorpion";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "186" AND name = "Vanguards_Wyvern";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Beasttender";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Constable";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Defender";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Drakekeeper";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Hatamoto";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Kusa";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Mason";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Militant";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Minstrel";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Protector";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Purloiner";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Thaumaturge";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Undertaker";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Vigilante";
UPDATE mob_groups SET dropid = "2558" WHERE zoneid = "186" AND name = "Vanguard_Vindicator";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "Vazhe_Pummelsong";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "ZeVho_Fallsplitter";
UPDATE mob_groups SET dropid = "2907" WHERE zoneid = "186" AND name = "ZoPha_Forgesoul";
