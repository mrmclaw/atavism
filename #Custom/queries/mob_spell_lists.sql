-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

-- Jade Sepulcher
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,530,1,255); -- Refueling - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,593,1,255); -- Magic Fruit - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,595,1,255); -- 1000 Needles - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,538,1,255); -- Memento Mori - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,594,1,255); -- Uppercut - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,641,1,255); -- Hysteric Barage - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,632,1,255); -- Diamondhide - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,642,1,255); -- Amplification - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,547,1,255); -- Cocoon - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,548,1,255); -- Filamented Hold - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,633,1,255); -- Enervation - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,540,1,255); -- Spinal Cleave - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,608,1,255); -- Frost Breath - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,640,1,255); -- Tail Slap - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,574,1,255); -- Feather Barrier - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,604,1,255); -- Bad Breath - BLU LB5

-- Uleguerand Range
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',470,152,1,255); -- Blizzard IV
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',470,181,1,255); -- Blizzaga III
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',470,356,1,255); -- Paralyga
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',470,362,1,255); -- Bindga

-- CoP 5-3U Where Messengers Gather (Boneyard Gully Fight)
	-- Shikaree_X Added all /nin spells up to BST50/NIN25
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,338,1,255); -- Utsusemi: Ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,320,1,255); -- Katon_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,323,1,255); -- hyoton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,326,1,255); -- huton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,329,1,255); -- doton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,332,1,255); -- raiton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,335,1,255); -- suiton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,347,1,255); -- kurayami_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',471,344,1,255); -- hojo_ichi
	-- Shikaree_Y Added most powerful DRK spells for level 50
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,144,1,255); -- Fire
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,149,1,255); -- Blizzard
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,154,1,255); -- Aero
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,160,1,255); -- Stone II	
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,164,1,255); -- Thunder	
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,170,1,255); -- Water II
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,220,1,255); -- Poison **REMOVE POISON 2 (221)**
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,231,1,255); -- Bio II
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,245,1,255); -- Drain
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,247,1,255); -- Aspir
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,252,1,255); -- Stun
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,266,1,255); -- Absorb-STR
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,267,1,255); -- Absorb-DEX
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,268,1,255); -- Absorb-VIT
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,269,1,255); -- Absorb-AGI
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,270,1,255); -- Absorb-INT
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,271,1,255); -- Absorb-MND
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,272,1,255); -- Absorb-CHR
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',472,275,1,255); -- Absorb-TP
	-- Shikaree_Z Added all /whm spells up to DRG50/WHM25
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,3,1,255); -- Cure III	
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,15,1,255); -- Paralyna
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,17,1,255); -- Silena
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,14,1,255); -- Poisona
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,16,1,255); -- Blindna
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,23,1,255); -- Dia
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,28,1,255); -- Banish
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,33,1,255); -- Diaga
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,38,1,255); -- Banishga
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,43,1,255); -- Protect
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,48,1,255); -- Shell
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,53,1,255); -- Blink
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,55,1,255); -- Aquaveil
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,56,1,255); -- Slow
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,58,1,255); -- Paralyze
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',473,59,1,255); -- Silence

-- CoP 5-3L Past Sins (Mineshaft 2716 Fight)
	-- Chekochuk Spell List (Used three different FFXI Captures) https://youtu.be/lM65yVpKVGw   https://youtu.be/KM6KAX2iAQ0   https://youtu.be/uhLEnV-3VXs
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,146,1,255); -- Fire III
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,156,1,255); -- Aero III
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,171,1,255); -- Water III
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,195,1,255); -- Thundaga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,204,1,255); -- Flare
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,210,1,255); -- Quake
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,212,1,255); -- Burst
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,214,1,255); -- Flood
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,231,1,255); -- Bio II
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,235,1,255); -- Burn
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,236,1,255); -- Frost
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,237,1,255); -- Choke
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,240,1,255); -- Drown
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,245,1,255); -- Drain
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,247,1,255); -- Aspir
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,249,1,255); -- Blaze Spikes
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,252,1,255); -- Stun
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,253,1,255); -- Sleep
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,254,1,255); -- Blind
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,258,1,255); -- Bind
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,259,1,255); -- Sleep II
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,273,1,255); -- Sleepga
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',474,274,1,255); -- Sleepga II
	-- Movamuq Spell List (Used three different FFXI Captures) https://youtu.be/lM65yVpKVGw   https://youtu.be/KM6KAX2iAQ0   https://youtu.be/uhLEnV-3VXs
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,5,1,255); -- Cure V
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,14,1,255); -- Poisona
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,15,1,255); -- Paralyna
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,21,1,255); -- Holy
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,24,1,255); -- Dia II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,29,1,255); -- Banish II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,34,1,255); -- Diaga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,39,1,255); -- Banishga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,45,1,255); -- Protect III
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,46,1,255); -- Protect IV
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,50,1,255); -- Shell III
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,53,1,255); -- Blink
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,54,1,255); -- Stoneskin
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,55,1,255); -- Aquaveil
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,56,1,255); -- Slow
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,57,1,255); -- Haste
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,58,1,255); -- Paralyze
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,59,1,255); -- Silence
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',475,112,1,255); -- Flash
	-- Trikotrak Spell List (Used three different FFXI Captures) https://youtu.be/lM65yVpKVGw   https://youtu.be/KM6KAX2iAQ0   https://youtu.be/uhLEnV-3VXs
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,4,1,255); -- Cure IV
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,24,1,255); -- Dia II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,34,1,255); -- Diaga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,50,1,255); -- Shell III
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,53,1,255); -- Blink
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,54,1,255); -- Stoneskin
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,55,1,255); -- Aquaveil
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,56,1,255); -- Slow
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,57,1,255); -- Haste
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,105,1,255); -- Enwater
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,108,1,255); -- Regen
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,145,1,255); -- Fire II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,150,1,255); -- Blizzard II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,155,1,255); -- Aero II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,160,1,255); -- Stone II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,165,1,255); -- Thunder II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,170,1,255); -- Water II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,216,1,255); -- Gravity
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,221,1,255); -- Poison II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,231,1,255); -- Bio II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,253,1,255); -- Sleep
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,254,1,255); -- Blind
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,258,1,255); -- Bind
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,259,1,255); -- Sleep II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',476,260,1,255); -- Dispel
-- BCNM 30 Carapace Combatants
INSERT IGNORE INTO mob_spell_lists VALUES ('Pilwiz',477,190,1,255); -- Stonega II

-- CoP 2-5 Ancient Vows
UPDATE mob_spell_lists SET min_level = 43, max_level = 44 WHERE spell_list_id = 29 AND spell_id = 174; -- Allows casting in staff form
UPDATE mob_spell_lists SET min_level = 43, max_level = 44 WHERE spell_list_id = 29 AND spell_id = 179; -- Allows casting in staff form
UPDATE mob_spell_lists SET min_level = 43, max_level = 44 WHERE spell_list_id = 29 AND spell_id = 184; -- Allows casting in staff form
UPDATE mob_spell_lists SET min_level = 43, max_level = 44 WHERE spell_list_id = 29 AND spell_id = 189; -- Allows casting in staff form
UPDATE mob_spell_lists SET min_level = 43, max_level = 44 WHERE spell_list_id = 29 AND spell_id = 194; -- Allows casting in staff form
UPDATE mob_spell_lists SET min_level = 43, max_level = 44 WHERE spell_list_id = 29 AND spell_id = 199; -- Allows casting in staff form

-- CoP 4-3 Old Professor & Minions
INSERT IGNORE INTO mob_spell_lists VALUES ('Old_Professor_Mariselle',478,252,1,255); -- stun
INSERT IGNORE INTO mob_spell_lists VALUES ('Old_Professor_Mariselle',478,270,1,255); -- absorb-int
INSERT IGNORE INTO mob_spell_lists VALUES ('Old_Professor_Mariselle',478,259,1,255); -- sleep_ii
INSERT IGNORE INTO mob_spell_lists VALUES ('Old_Professor_Mariselle',478,245,1,255); -- drain
INSERT IGNORE INTO mob_spell_lists VALUES ('Old_Professor_Mariselle',478,258,1,255); -- bind
INSERT IGNORE INTO mob_spell_lists VALUES ('Old_Professor_Mariselle',478,274,1,255); -- sleepga_ii
INSERT IGNORE INTO mob_spell_lists VALUES ('Mariselles_Pupil',479,216,1,255); -- gravity
INSERT IGNORE INTO mob_spell_lists VALUES ('Mariselles_Pupil',479,254,1,255); -- blind
INSERT IGNORE INTO mob_spell_lists VALUES ('Mariselles_Pupil',479,245,1,255); -- drain

-- CoP 7-5
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 4, 1, 255); -- Cure IV
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 21, 1, 255); -- Holy
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 34, 1, 255); -- Diaga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 39, 1, 255); -- Banishga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 46, 1, 255); -- Protect IV
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 50, 1, 255); -- Shell III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 56, 1, 255); -- Slow
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 57, 1, 255); -- Haste
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 58, 1, 255); -- Paralyze
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 59, 1, 255); -- Silence
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 111, 1, 255); -- Regen III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Cherukiki_COP', 458, 112, 1, 255); -- Flash
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 146, 1, 255); -- Fire III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 151, 1, 255); -- Blizzard III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 161, 1, 255); -- Stone III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 166, 1, 255); -- Thunder III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 175, 1, 255); -- Firaga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 191, 1, 255); -- Stonega III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 195, 1, 255); -- Thundaga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 201, 1, 255); -- Waterga III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 226, 1, 255); -- Poisonga II
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 232, 1, 255); -- Bio III
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 235, 1, 255); -- Burn
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 236, 1, 255); -- Frost
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 237, 1, 255); -- Choke
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 238, 1, 255); -- Rasp 
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 239, 1, 255); -- Shock
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 240, 1, 255); -- Drown
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 245, 1, 255); -- Drain
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 247, 1, 255); -- Aspir
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 258, 1, 255); -- Bind
INSERT IGNORE INTO `mob_spell_lists` VALUES('Kukki_Chebukki_COP', 459, 274, 1, 255); -- Sleepga II
