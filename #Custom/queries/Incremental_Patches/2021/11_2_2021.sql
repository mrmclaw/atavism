-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

-- Bastok 7-2 mission tunning
UPDATE mob_groups SET HP = 5500 WHERE name = "Go_Bha_Slaughterer" and zoneid = 144; -- Hp increase
UPDATE mob_groups SET HP = 5500 WHERE name = "Ku_Jhu_Graniteskin" and zoneid = 144; -- Hp increase
UPDATE mob_groups SET HP = 5500 WHERE name = "Da_Shu_Knightslayer" and zoneid = 144; -- Hp increase
UPDATE mob_groups SET HP = 5500 WHERE name = "Sa_Nha_Soulsaver" and zoneid = 144; -- Hp increase

UPDATE mob_pools SET entityFlags = "4" WHERE name = "Go_Bha_Slaughterer"; -- Model size should be bigger.
UPDATE mob_pools SET entityFlags = "4" WHERE name = "Ku_Jhu_Graniteskin"; -- Model size should be bigger.
UPDATE mob_pools SET entityFlags = "4" WHERE name = "Da_Shu_Knightslayer"; -- Model size should be bigger.
UPDATE mob_pools SET entityFlags = "4" WHERE name = "Sa_Nha_Soulsaver"; -- Model size should be bigger.


-- Fixing Tenshodo vendor stock
UPDATE guild_shops SET daily_increase = 5, initial_quantity = 10 WHERE guildid = 60423 AND itemid = 16406; -- baghnakhs
UPDATE guild_shops SET max_quantity = 297, daily_increase = 99, initial_quantity = 99 WHERE guildid = 60423 AND itemid = 17340; -- bullet
UPDATE guild_shops SET daily_increase = 5, initial_quantity = 10 WHERE guildid = 60423 AND itemid = 16411; -- claws
UPDATE guild_shops SET max_quantity = 297, daily_increase = 99, initial_quantity = 99 WHERE guildid = 60423 AND itemid = 17322; -- fire arrow
UPDATE guild_shops SET max_quantity = 297, daily_increase = 99, initial_quantity = 99 WHERE guildid = 60423 AND itemid = 17302; -- juji_shuriken
UPDATE guild_shops SET daily_increase = 5, initial_quantity = 10 WHERE guildid = 60423 AND itemid = 16871; -- kamayari
UPDATE guild_shops SET daily_increase = 5, initial_quantity = 10 WHERE guildid = 60423 AND itemid = 17802; -- kiku-ichimonji
UPDATE guild_shops SET daily_increase = 5, initial_quantity = 10 WHERE guildid = 60423 AND itemid = 16988; -- kotetsu
UPDATE guild_shops SET daily_increase = 5, initial_quantity = 10 WHERE guildid = 60423 AND itemid = 16419; -- patas
UPDATE guild_shops SET daily_increase = 5, initial_quantity = 10 WHERE guildid = 60423 AND itemid = 17259; -- pirate's gun
UPDATE guild_shops SET max_quantity = 297, daily_increase = 99, initial_quantity = 99 WHERE guildid = 60431 AND itemid = 17340; -- bullet
UPDATE guild_shops SET max_quantity = 297, daily_increase = 99, initial_quantity = 99 WHERE guildid = 60431 AND itemid = 17322; -- fire arrow
UPDATE guild_shops SET max_quantity = 297, daily_increase = 99, initial_quantity = 99 WHERE guildid = 60431 AND itemid = 17304; -- fuma shuriken
UPDATE guild_shops SET max_quantity = 297, daily_increase = 99, initial_quantity = 99 WHERE guildid = 60431 AND itemid = 17302; -- juji_shuriken
INSERT INTO `guild_shops` VALUES (60431,16871,183516,404395,20,5,10); -- kamayari
INSERT INTO `guild_shops` VALUES (60431,17802,189945,464059,20,5,10); -- kiku-ichimonji
INSERT INTO `guild_shops` VALUES (60431,16988,14676,16808,20,5,10); -- kotetsu
INSERT INTO `guild_shops` VALUES (60431,16987,12253,14033,20,5,10); -- okanehira
INSERT INTO `guild_shops` VALUES (60431,16419,76419,168396,20,5,10); -- patas
INSERT INTO `guild_shops` VALUES (60431,17259,72144,158976,20,5,10); -- pirate's gun

-- Bloodthread adjustments
DELETE FROM mob_droplist WHERE mob_droplist.itemId = 1700 AND mob_droplist.dropId = 660; -- Diremite Dominator no longer drops bloodthread.
UPDATE mob_droplist SET itemRate = 240 WHERE itemId = 1700 AND dropId = 661; -- Diremite Stalker drop chance increased

-- Mail 2 account fixes
UPDATE item_basic SET flags = 45124 WHERE itemid = 1195; -- flask_of_romaeve_spring_water
UPDATE item_basic SET flags = 45124 WHERE itemid = 1404; -- seal_of_genbu
UPDATE item_basic SET flags = 45124 WHERE itemid = 1405; -- seal_of_seiryu
UPDATE item_basic SET flags = 45124 WHERE itemid = 1406; -- seal_of_byakko
UPDATE item_basic SET flags = 45124 WHERE itemid = 1407; -- seal_of_suzaku
UPDATE item_basic SET flags = 45124 WHERE itemid = 1418; -- gem_of_the_east
UPDATE item_basic SET flags = 45124 WHERE itemid = 1419; -- springstone
UPDATE item_basic SET flags = 45124 WHERE itemid = 1420; -- gem_of_the_south
UPDATE item_basic SET flags = 45124 WHERE itemid = 1421; -- summerstone
UPDATE item_basic SET flags = 45124 WHERE itemid = 1422; -- gem_of_the_west
UPDATE item_basic SET flags = 45124 WHERE itemid = 1423; -- autumnstone
UPDATE item_basic SET flags = 45124 WHERE itemid = 1424; -- gem_of_the_north
UPDATE item_basic SET flags = 45124 WHERE itemid = 1425; -- winterstone
UPDATE item_basic SET flags = 12356 WHERE itemid = 1847; -- fifth_virtue
UPDATE item_basic SET flags = 12356 WHERE itemid = 1848; -- fourth_virtue
UPDATE item_basic SET flags = 12356 WHERE itemid = 1849; -- sixth_virtue
UPDATE item_basic SET flags = 12356 WHERE itemid = 1850; -- first_virtue
UPDATE item_basic SET flags = 12356 WHERE itemid = 1851; -- deed_of_placidity
UPDATE item_basic SET flags = 12356 WHERE itemid = 1852; -- high-quality_phuabo_organ
UPDATE item_basic SET flags = 12356 WHERE itemid = 1853; -- second_virtue
UPDATE item_basic SET flags = 12356 WHERE itemid = 1854; -- deed_of_moderation
UPDATE item_basic SET flags = 12356 WHERE itemid = 1855; -- high-quality_xzomit_organ
UPDATE item_basic SET flags = 12356 WHERE itemid = 1856; -- third_virtue
UPDATE item_basic SET flags = 12356 WHERE itemid = 1870; -- deed_of_sensibility
UPDATE item_basic SET flags = 12356 WHERE itemid = 1871; -- high-quality_hpemde_organ
UPDATE item_basic SET flags = 12356 WHERE itemid = 1872; -- ghrah_m_chip
UPDATE item_basic SET flags = 12356 WHERE itemid = 1899; -- high-quality_euvhi_organ
UPDATE item_basic SET flags = 12356 WHERE itemid = 1900; -- high-quality_aern_organ
UPDATE item_basic SET flags = 45120 WHERE itemid = 1904; -- ivory_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1905; -- scarlet_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1906; -- emerald_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1907; -- silver_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1908; -- cerulean_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1909; -- smalt_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1910; -- smoky_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1986; -- orchid_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1987; -- charcoal_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 1988; -- magenta_chip
UPDATE item_basic SET flags = 45120 WHERE itemid = 2127; -- metal_chip
UPDATE item_basic SET flags = 47172 WHERE itemid = 16575; -- curtana