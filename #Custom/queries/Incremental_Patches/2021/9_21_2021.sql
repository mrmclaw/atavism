-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------

-- Sewer Syrup
DELETE FROM `nm_spawn_points` WHERE mobid = 17461307; -- Delete old spawn points
INSERT IGNORE INTO `nm_spawn_points` VALUES (17461307, 0, -21.04, 0.9251, -340.1736); -- Add new spawn point
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "16480" AND dropType != 2  AND dropType != 4  AND dropID  = "2298"; -- *thiefs_knife* from *Sozu_Rogberry* was *1000* 

UPDATE mob_groups SET respawntime = 129600 WHERE name = "Garfurlar_the_Rabid"; -- Set respawn time 36 hours until re-coded
UPDATE mob_groups SET respawntime = 129600 WHERE name = "Garharlor_the_Unruly"; -- Set respawn time 36 hours until re-coded
UPDATE mob_groups SET respawntime = 129600 WHERE name = "Garhorlur_the_Brutal"; -- Set respawn time 36 hours until re-coded

-- Adding BB items to NQ Kings. Also reordering Fafnir loot table so Wyrm Beard has a chance to drop

INSERT INTO `mob_droplist` VALUES(251, 0, 0, 1000, 1527, 50); -- Adding Behemoth Tongue to Behemoth loot table
INSERT INTO `mob_droplist` VALUES(21, 0, 0, 1000, 1525, 50); -- Adding Adamantoise Egg to Adamantoise loot table
DELETE FROM mob_droplist WHERE mob_droplist.dropid = 805;
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 1526, 50); -- Adding Wyrm Beard to Fafnir loot table
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 867, 750);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 13914, 500);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 14075, 570);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 16555, 60);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 16942, 290);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 17653, 270);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 1321, 240);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 1326, 310);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 1328, 240);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 1339, 410);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 903, 900);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 1133, 20);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 4272, 30);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 4486, 410);
INSERT INTO `mob_droplist` VALUES(805, 0, 0, 1000, 3340, 100);
UPDATE mob_droplist SET itemRate = "750" WHERE itemID = "867" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *handful_of_dragon_scales* from *Fafnir* was *750*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "13914" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *aegishjalmr* from *Fafnir* was *500*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "14075" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *andvaranauts* from *Fafnir* was *570*
UPDATE mob_droplist SET itemRate = "50" WHERE itemID = "16555" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *ridill* from *Fafnir* was *180*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "16942" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *balmung* from *Fafnir* was *290*
UPDATE mob_droplist SET itemRate = "100" WHERE itemID = "17653" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *hrotti* from *Fafnir* was *270*
UPDATE mob_droplist SET itemRate = "500" WHERE itemID = "903" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *dragon_talon* from *Fafnir* was *900*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "1133" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *vial_of_dragon_blood* from *Fafnir* was *20*
UPDATE mob_droplist SET itemRate = "10" WHERE itemID = "4272" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *slice_of_dragon_meat* from *Fafnir* was *30*
UPDATE mob_droplist SET itemRate = "240" WHERE itemID = "4486" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *dragon_heart* from *Fafnir* was *410*
UPDATE mob_droplist SET itemRate = "0" WHERE itemID = "3340" AND dropType != 2  AND dropType != 4  AND dropID  = "805"; -- *cup_of_sweet_tea* from *Fafnir* was *100*
DELETE FROM mob_droplist WHERE itemID = "1328" AND dropType != 2  AND dropType != 4  AND dropID = "805"; -- Delete aqua hands
DELETE FROM mob_droplist WHERE itemID = "1326" AND dropType != 2  AND dropType != 4  AND dropID = "805"; -- Delete aqua feet
DELETE FROM mob_droplist WHERE itemID = "1321" AND dropType != 2  AND dropType != 4  AND dropID = "805"; -- Delete earth hands
DELETE FROM mob_droplist WHERE itemID = "1339" AND dropType != 2  AND dropType != 4  AND dropID = "805"; -- Delete nept head
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 1, 500, 1328, 250); -- Add aqua hands for shared loot 1 
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 1, 500, 1326, 250); -- Add aqua feet for shared loot 1 
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 1, 500, 1321, 250); -- Add earth hands for shared loot 1 
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 1, 500, 1339, 250); -- Add nept head for shared loot 1 
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 2, 500, 1328, 250); -- Add aqua hands for shared loot 1 
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 2, 500, 1326, 250); -- Add aqua feet for shared loot 1 
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 2, 500, 1321, 250); -- Add earth hands for shared loot 1 
INSERT IGNORE INTO mob_droplist VALUES(805, 1, 2, 500, 1339, 250); -- Add nept head for shared loot 1 

-- Nashmau ship issue
UPDATE npc_list SET name = 0x14, speed = 50, speedsub = 50 WHERE npcid  = 16994327;
UPDATE npc_list SET name = 0x7 WHERE npcid  = 17191527;


UPDATE mob_groups SET respawntime = 0, spawntype = 128, HP = 15000, dropid = 0 WHERE name = "Garfurlar_the_Rabid"; -- HP and spawn changes - only Uruly should drop items
UPDATE mob_groups SET respawntime = 0, spawntype = 128, HP = 15000 WHERE name = "Garharlor_the_Unruly"; -- HP and spawn changes - only Uruly should drop items
UPDATE mob_groups SET respawntime = 0, spawntype = 128, HP = 18000 WHERE name = "Garhorlur_the_Brutal"; -- HP and spawn changes - only Uruly should drop items
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "15895" AND dropType != 2  AND dropType != 4  AND dropID  = "929"; -- *trance_belt* from *Garharlor_the_Unruly* was *100*
UPDATE mob_droplist SET itemRate = "150" WHERE itemID = "14948" AND dropType != 2  AND dropType != 4  AND dropID  = "929"; -- *genie_gages* from *Garharlor_the_Unruly* was *100*
