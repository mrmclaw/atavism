-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----- NATION IDS -------
-- 0 SANDORIA
-- 1 BASTOK
-- 2 WINDURST


-- ----- REGION IDS -------
-- 0  RONFAURE
-- 1  ZULKHEIM
-- 2  NORVALLEN
-- 3  GUSTABERG
-- 4  DERFLAND
-- 5  SARUTABARUTA
-- 6  KOLSHUSHU
-- 7  ARAGONEU
-- 8  FAUREGANDI
-- 9 VALDEAUNIA
-- 10 QUFIMISLAND
-- 11 LITELOR
-- 12 KUZOTZ
-- 13 VOLLBOW
-- 14 ELSHIMOLOWLANDS
-- 15 ELSHIMOUPLANDS
-- 16 TU'LIA
-- 17 MOVALPOLOS
-- 18 TAVNAZIAN


-- 0  RONFAURE
UPDATE conquest_system SET sandoria_influence = "2000" WHERE region_id = 0; -- DOMINANT
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 0;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 0;
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 0;

-- 1  ZULKHEIM
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 1;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 1;
UPDATE conquest_system SET windurst_influence = "2000" WHERE region_id = 1; -- DOMINANT
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 1;

-- 2  NORVALLEN
UPDATE conquest_system SET sandoria_influence = "2000" WHERE region_id = 2; -- DOMINANT
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 2;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 2;
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 2;

-- 3  GUSTABERG
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 3;
UPDATE conquest_system SET bastok_influence = "2000" WHERE region_id = 3; -- DOMINANT
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 3;
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 3;

-- 4  DERFLAND
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 4;
UPDATE conquest_system SET bastok_influence = "2000" WHERE region_id = 4; -- DOMINANT
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 4;
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 4;

-- 5  SARUTABARUTA
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 5;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 5;
UPDATE conquest_system SET windurst_influence = "2000" WHERE region_id = 5; -- DOMINANT
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 5;

-- 6  KOLSHUSHU
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 6;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 6;
UPDATE conquest_system SET windurst_influence = "2000" WHERE region_id = 6; -- DOMINANT
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 6;

-- 7  ARAGONEU
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 7;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 7;
UPDATE conquest_system SET windurst_influence = "2000" WHERE region_id = 7;
UPDATE conquest_system SET beastmen_influence = "500" WHERE region_id = 7; -- DOMINANT

-- 8  FAUREGANDI
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 8;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 8;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 8;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 8; -- DOMINANT

-- 9 VALDEAUNIA
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 9;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 9;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 9;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 9; -- DOMINANT

-- 10 QUFIMISLAND
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 10;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 10;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 10;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 10; -- DOMINANT

-- 11 LITELOR
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 11;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 11;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 11;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 11; -- DOMINANT

-- 12 KUZOTZ
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 12;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 12;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 12;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 12; -- DOMINANT

-- 13 VOLLBOW
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 13;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 13;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 13;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 13; -- DOMINANT

-- 14 ELSHIMOLOWLANDS
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 14;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 14;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 14;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 14; -- DOMINANT

-- 15 ELSHIMOUPLANDS
UPDATE conquest_system SET sandoria_influence = "1000" WHERE region_id = 15;
UPDATE conquest_system SET bastok_influence = "1000" WHERE region_id = 15;
UPDATE conquest_system SET windurst_influence = "1000" WHERE region_id = 15;
UPDATE conquest_system SET beastmen_influence = "1500" WHERE region_id = 15; -- DOMINANT

-- 16 TU'LIA
UPDATE conquest_system SET sandoria_influence = "5000" WHERE region_id = 16; -- DOMINANT
UPDATE conquest_system SET bastok_influence = "0" WHERE region_id = 16;
UPDATE conquest_system SET windurst_influence = "0" WHERE region_id = 16;
UPDATE conquest_system SET beastmen_influence = "1000" WHERE region_id = 16;

-- 17 MOVALPOLOS
UPDATE conquest_system SET sandoria_influence = "0" WHERE region_id = 17;
UPDATE conquest_system SET bastok_influence = "0" WHERE region_id = 17; -- DOMINANT
UPDATE conquest_system SET windurst_influence = "0" WHERE region_id = 17;
UPDATE conquest_system SET beastmen_influence = "1000" WHERE region_id = 17;

-- 18 TAVNAZIAN
UPDATE conquest_system SET sandoria_influence = "0" WHERE region_id = 18;
UPDATE conquest_system SET bastok_influence = "0" WHERE region_id = 18;
UPDATE conquest_system SET windurst_influence = "0" WHERE region_id = 18; -- DOMINANT
UPDATE conquest_system SET beastmen_influence = "1000" WHERE region_id = 18;