-- --------------------------------
-- Dynamis-Sandoria SQL Drop Tables --
-- --------------------------------

-- Dynamis Sandoria

-- Normal Beastmen Mobs - Vanguards (2548)
DELETE FROM mob_droplist WHERE dropid = "2548" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15132, 72); -- Adds WAR Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15118, 72); -- Adds MNK Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15074, 72); -- Adds WHM Head
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15136, 72); -- Adds RDM Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15108, 72); -- Adds PLD Hands
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15125, 72); -- Adds BST Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15081, 71); -- Adds BRD Head
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15127, 71); -- Adds RNG Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15129, 71); -- Adds Nin Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15145, 71); -- Adds DRG Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15146, 71); -- Adds SMN Feet
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 15025, 71); -- Adds BLU Hands
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 16349, 71); -- Adds COR Legs
INSERT INTO mob_droplist VALUES(2548, 1, 1, 50, 11388, 71); -- Adds PUP Feet
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1519, 10); -- Adds fresh_orc_liver
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1517, 10); -- Adds giant_frozen_head
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1516, 10); -- Adds griffon_hide
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1452, 240); -- Adds 1 bronzepiece 
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1452, 150); -- Adds 2 bronzepiece 
INSERT INTO mob_droplist VALUES(2548, 0, 0, 1000, 1452, 100); -- Adds 3 bronzepiece
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18308, 250); -- Adds ihintanto
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18332, 250); -- Adds relic gun	
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18290, 250); -- Adds relic bhuj
INSERT INTO mob_droplist VALUES(2548, 1, 2, 50, 18296, 250); -- Adds relic lance

-- NM Beastmen Mobs - 237
DELETE FROM mob_droplist WHERE dropid = "237" AND droptype != 2; -- Delete
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15132, 72); -- Adds WAR Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15118, 72); -- Adds MNK Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15074, 72); -- Adds WHM Head
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15136, 72); -- Adds RDM Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15108, 72); -- Adds PLD Hands
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15125, 72); -- Adds BST Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15081, 71); -- Adds BRD Head
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15127, 71); -- Adds RNG Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15129, 71); -- Adds Nin Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15145, 71); -- Adds DRG Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15146, 71); -- Adds SMN Feet
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 15025, 71); -- Adds BLU Hands
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 16349, 71); -- Adds COR Legs
INSERT INTO mob_droplist VALUES(237, 1, 1, 50, 11388, 71); -- Adds PUP Feet
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1519, 10); -- Adds fresh_orc_liver
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1517, 10); -- Adds giant_frozen_head
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1516, 10); -- Adds griffon_hide
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1470, 50); -- Adds sparkling stone
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1452, 240); -- Adds 1 bronzepiece 
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1452, 150); -- Adds 2 bronzepiece
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1452, 100); -- Adds 3 bronzepiece
INSERT INTO mob_droplist VALUES(237, 0, 0, 1000, 1453, 50); -- Adds 1 Mont

-- Tombstones - 2201
DELETE FROM mob_droplist WHERE dropid = "2201"; -- Delete
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 749, 50); -- Add myth coin
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 748, 50); -- Add gold coin
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 1474, 100); -- Add infinity core
INSERT INTO mob_droplist VALUES(2201, 0, 0, 1000, 1453, 50); -- Adds 1 Mont

-- Megaboss - 1967
DELETE FROM mob_droplist WHERE dropid = "1967"; -- Delete
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 749, 100); -- Add myth coin
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 748, 100); -- Add gold coin
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1474, 150); -- Add infinity core
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1452, 150); -- Adds 1 bronzepiece 
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1452, 150); -- Adds 2 bronzepiece 
INSERT INTO mob_droplist VALUES(1967, 0, 0, 1000, 1453, 150); -- Adds 1 Mont

-- assign correct drop pools to each mob 
UPDATE mob_groups SET dropid = "1967" WHERE zoneid = "185" AND name = "Arch_Overlord_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Battlechoir_Gitchfotch";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Bladeburner_Rokgevok";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Bloodfist_Voshgrosh";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Djokvukks_Wyvern";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Kratzvatzs_Hecteyes";
UPDATE mob_groups SET dropid = "1967" WHERE zoneid = "185" AND name = "Overlords_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Reapertongue_Gadgquok";
UPDATE mob_groups SET dropid = "2201" WHERE zoneid = "185" AND name = "Serjeant_Tombstone";
UPDATE mob_groups SET dropid = "2201" WHERE zoneid = "185" AND name = "Serjeant_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Soulsender_Fugbrag";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Spellspear_Djokvukk";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Steelshank_Kratzvatz";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Vanguards_Avatar";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Vanguards_Hecteyes";
UPDATE mob_groups SET dropid = "0" WHERE zoneid = "185" AND name = "Vanguards_Wyvern";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Amputator";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Backstabber";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Bugler";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Dollmaster";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Footsoldier";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Grappler";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Gutslasher";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Hawker";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Impaler";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Mesmerizer";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Neckchopper";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Pillager";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Predator";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Trooper";
UPDATE mob_groups SET dropid = "2548" WHERE zoneid = "185" AND name = "Vanguard_Vexer";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Voidstreaker_Butchnotch";
UPDATE mob_groups SET dropid = "2201" WHERE zoneid = "185" AND name = "Warchief_Tombstone";
UPDATE mob_groups SET dropid = "237" WHERE zoneid = "185" AND name = "Wyrmgnasher_Bjakdek";
