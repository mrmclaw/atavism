-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- GENERAL --
-- ----------

-- H2H
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "combo"; -- Was set to 5
UPDATE weapon_skills SET jobs = X'00010000000000000000000000000000000000000000' WHERE name = "asuran_fists"; -- Was set to MNK and PUP (should be only MNK)

-- Dagger
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "wasp_sting"; -- Was set to 5
UPDATE weapon_skills SET jobs = X'01000001010101010101010101010100010101010000' WHERE name = "energy_steal"; -- Available as main job only

-- Sword
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "fast_blade"; -- Was set to 5
UPDATE weapon_skills SET jobs = X'02000000020002020000000000000002000000000002' WHERE name = "energy_steal"; -- Available as main job only
UPDATE weapon_skills SET jobs = X'02000000000002020000000000000002000000000002' WHERE name = "red_lotus_blade"; -- RDM shouldn't have this
UPDATE weapon_skills SET jobs = X'02000000000002020000000000000002000000000002' WHERE name = "seraph_blade"; -- RDM shouldn't have this
UPDATE weapon_skills SET jobs = X'02000000000002020000000000000002000000000002' WHERE name = "vorpal_blade"; -- RDM shouldn't have this

-- Greatsword
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "hard_slash"; -- Was set to 5

-- Axe
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "raging_axe"; -- Was set to 5

-- Great Axe
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "shield_break"; -- Was set to 5

-- Scythe
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "slice"; -- Was set to 5

-- Polearm
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "double_thrust"; -- Was set to 5

-- Katana
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "blade_rin"; -- Was set to 5

-- Great Katana
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "tachi_enpi"; -- Was set to 5

-- Club
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "shining_strike"; -- Was set to 5

-- Staves
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "heavy_swing"; -- Was set to 5

-- Archery
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "flaming_arrow"; -- Was set to 5

-- Markmanship
UPDATE weapon_skills SET skilllevel = "10" WHERE name = "hot_shot"; -- Was set to 5
