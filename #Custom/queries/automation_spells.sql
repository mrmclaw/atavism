-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- PUP Fixes
UPDATE automaton_spells SET heads = 32 WHERE spellid = 147;
UPDATE automaton_spells SET heads = 32 WHERE spellid = 152;
UPDATE automaton_spells SET heads = 32 WHERE spellid = 157;
UPDATE automaton_spells SET heads = 32 WHERE spellid = 162;
UPDATE automaton_spells SET heads = 32 WHERE spellid = 172;

