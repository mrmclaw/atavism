-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- Nation item latents
INSERT IGNORE INTO `item_latents` VALUES(16799, 25, 7, 44, 20); -- senior_gold_musketeers_scythe
INSERT IGNORE INTO `item_latents` VALUES(16893, 1, 10, 44, 19); -- reserve_captains_lance
INSERT IGNORE INTO `item_latents` VALUES(16953, 25, 7, 44, 19); -- reserve_captains_greatsword
INSERT IGNORE INTO `item_latents` VALUES(17457, 71, 7, 44, 20); -- senior_gold_musketeers_rod
INSERT IGNORE INTO `item_latents` VALUES(17458, 71, 7, 44, 19); -- reserve_captains_mace
INSERT IGNORE INTO `item_latents` VALUES(17508, 23, 10, 44, 21); -- master_casters_baghnakhs
INSERT IGNORE INTO `item_latents` VALUES(17530, 71, 8, 44, 21); -- master_casters_pole
INSERT IGNORE INTO `item_latents` VALUES(17617, 1, 10, 44, 21); -- master_casters_knife
INSERT IGNORE INTO `item_latents` VALUES(17655, 1, 10, 44, 20); -- senior_gold_musketeers_scimitar
INSERT IGNORE INTO `item_latents` VALUES(17934, 23, 10, 44, 19); -- reserve_captains_pick
INSERT IGNORE INTO `item_latents` VALUES(18145, 26, 7, 44, 21); -- master_casters_bow
INSERT IGNORE INTO `item_latents` VALUES(18196, 23, 10, 44, 20); -- senior_gold_musketeers_axe

-- Sorcerers Ring Latent Fix
UPDATE item_latents SET latentId = 61 WHERE itemid = 13289;